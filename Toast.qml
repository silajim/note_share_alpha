import QtQuick 2.4

Rectangle {
    id: toast
    color: "#2C333C"
    opacity: 0

    property bool local: true
    property string text

    onWidthChanged: console.log("Toast width " + width)

    anchors.bottom: parent.bottom
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottomMargin: parent.height/9

    property bool  active: false
    readonly property real multFactor: 1.2
    radius: pixelDensity * 3

    function widthcalc(){
//        if (parent.width != undefined && parent.width > 0) {
//            // if(loader.item.paintedWidth*multFactor <  parent.width/multFactor){
//            //     console.log("TOAST LOADER ITEM WIDTH TAKEN")
//            //     width=loader.item.paintedWidth*multFactor
//            //     toast.width = width
//            //     local=true
//            // }else{
//                width = parent.width/multFactor
//                toast.width = width
//                local = false
//            // }
//        }
//        else {
//            width=app.screenSize.width / multFactor
//            toast.width = width
//            local=true
//        }
        width = parent.width/multFactor
        toast.width = width
        local = false
    }

    Loader{
        id: loader
        active: false
        anchors.fill: parent
        sourceComponent: Component{
            Item{
                id: item

                property alias paintedWidth: toast_text.paintedWidth
                property alias _height: toast_text.height
                //                MaterialShadow{
                //                    z:parent.z-1
                //                    asynchronous: true
                //                    depth: 1
                //                    rotation: 180
                //                    anchors.left: parent.left
                //                    anchors.top: parent.top
                //                    anchors.bottom: parent.bottom
                //                    //left
                //                }
                //                MaterialShadow{
                //                    z:parent.z-1
                //                    asynchronous: true
                //                    depth: 1
                //                    rotation: 180
                //                    anchors.top: parent.top
                //                    anchors.left: parent.left
                //                    anchors.right: parent.right
                //                    //top
                //                }
                //                MaterialShadow{
                //                    z:parent.z-1
                //                    asynchronous: true
                //                    depth: 1
                //                    anchors.right: parent.right
                //                    anchors.top: parent.top
                //                    anchors.bottom: parent.bottom
                //                    //right

                //                }
                //                MaterialShadow{
                //                    z:parent.z-1
                //                    depth: 1
                //                    asynchronous: true
                //                    anchors.bottom :  parent.bottom
                //                    //        anchors.left: parent.left
                //                    //        anchors.right: parent.right
                //                    width: parent.width
                //                    //bottom
                //                }

                Text {
                    id: toast_text
                    color: "white"
                    // width: local? paintedWidth : toast.width/1.1
                    width: toast.width / multFactor
                    text: toast.text

                    anchors.centerIn: parent
                    font.pointSize: 22
                    wrapMode: Text.WordWrap

                    onTextChanged: {
                        toast.widthcalc()
                    }

                    onWidthChanged: console.log("text Width " + width )

                    Component.onCompleted: toast.width = Qt.binding(function() {
                        if (toast.parent.width > 0 || toast.parent.width != undefined)
                            return Math.min(toast_text.paintedWidth*multFactor, toast.parent.width/multFactor)
                        else {
                            // return toast_text.paintedWidth*multFactor
                            return toast_text.width*multFactor
                        }
                    })
                }

                Component.onCompleted: toast.height = Qt.binding(function() { return _height * 1.5 })
            }
        }
    }

    Connections{
        ignoreUnknownSignals: true
        target: app.imapAdapter

        onSetToast:{
            console.log("Toast show")
            loader.active = true
            toast.text = toastMsg
            var len = toastMsg.split(' ').length
            time.interval= 900*len
            active = true
            time.start()
        }
    }
    states: [
        State {
            name: "Active"
            when: toast.active

        }
    ]
    transitions: [
        Transition {
            from: ""
            to: "Active"
            PropertyAnimation{
                target: toast
                properties: "opacity"; to: 1
                duration: 250
            }
        },
        Transition {
            from: "Active"
            to: ""
            PropertyAnimation{
                alwaysRunToEnd: true
                target: toast
                properties: "opacity"; to: 0
                duration: 1000
                onStopped: loader.active = false
            }

        }
    ]
    Timer{
        id: time
        interval: 3000
        //running: toast.active
        onTriggered: {
            console.log("trigered")
            active = false
        }
        onIntervalChanged: console.log("interval " + interval)
    }

    function show(txt){
        console.log("Toast show")
        loader.active = true
        toast.text = txt
        var len = txt.split(' ').length
        time.interval= 900*len
        active = true
        time.start()

    }

}

