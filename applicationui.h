#ifndef APPLICATIONUI_H
#define APPLICATIONUI_H

#include <QObject>
#include <QQmlApplicationEngine>

#include "notemanager.h"

#include "notemodel.h"
#include "readsharemodel.h"

class ApplicationUi : public QObject
{
    Q_OBJECT
public:
    explicit ApplicationUi(QObject *parent = 0);



    NoteManager *getManager() const;

signals:
    void shareLoaded();
    void loadShareSignal(QString creator, int id);
    void needLogin();
    void uimessage(QString messge);
    void exiting();
    void loginstateChanged(bool state);


public slots:
    NoteModel *getModel() const;
    void loadShare(QString creator, int id);
    void newShare();
    ReadShareModel *getreadmodel();
    void login(QString user , QString pass);

private:
    NoteManager *manager;
    QQmlApplicationEngine *engine;
    NoteModel *model;
    ReadShareModel *readmodel;


private slots:
    void ongotAllSshares(sharelist shares);
    void onNewShare(Share sh);
    void onUpdateShare(Share sh);
    void onShareLoaded(Share sh);

};

#endif // APPLICATIONUI_H
