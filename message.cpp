#include "message.h"

Message::Message()
{

}

QList<QByteArray> Message::getChangelog()
{
    return changelog;
}

void Message::setChangelog(const QList<QByteArray> value)
{
    changelog = value;
}

QString Message::getFrom() const
{
    return from;
}

void Message::setFrom(const QString value)
{
    from = value;
}

QString Message::getText() const
{
    return text;
}

void Message::setText(const QString value)
{
    text = value;
}

QStringList Message::getCc() const
{
    return cc;
}

void Message::setCc(const QStringList value)
{
    cc = value;
}

QString Message::getRecipient() const
{
    return recipient;
}

void Message::setRecipient(const QString &value)
{
    recipient = value;
}

