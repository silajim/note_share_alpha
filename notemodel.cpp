#include "notemodel.h"

#include "debug.h"

NoteModel::NoteModel()
{

}



QHash<int, QByteArray> NoteModel::roleNames() const
{
     QHash<int, QByteArray> roles;
     roles[creatorRole] = "creator";
     roles[nameRole] = "name";
     roles[versionRole] = "version";
     roles[idRole] = "id";
     return roles;
}

QVariant NoteModel::data(const QModelIndex &index, int role) const
{

    if(index.row()>modeldata.size())
        return QVariant();

    switch (role) {
    case creatorRole:
        return modeldata.at(index.row())->creator;
        break;
    case nameRole:
          return modeldata.at(index.row())->name;
        break;
    case versionRole:
          return modeldata.at(index.row())->version;
        break;
    case idRole:
          return modeldata.at(index.row())->id;
        break;
    default:
        qdebug << "Index " << index.row() << "Default";
        return QVariant();
        break;
    }
}

int NoteModel::rowCount(const QModelIndex &parent) const
{
    return modeldata.size();
}

void NoteModel::insertShare(Share &sh)
{

    qdebug << "Insert Share " << sh.subject;

    NotemodelData *data = new NotemodelData();
    data->creator =sh.creator;
    data->name = sh.subject;
    data->id = sh.id;
    data->version = sh.version;

    beginInsertRows(QModelIndex(), modeldata.size(), modeldata.size());
    modeldata << data;
    endInsertRows();
}

void NoteModel::updateShare(Share &sh)
{
    for(int i=0;i<modeldata.size();i++){
        NotemodelData *data = modeldata[i];
        if(data->creator==sh.creator && data->id==sh.id && data->version<sh.version){
            data->name = sh.subject;
            data->version = sh.version;
            QModelIndex index = createIndex(i,0);
            dataChanged(index,index);
            return;
        }
    }
}



QString NotemodelData::getCreator() const
{
    return creator;
}

void NotemodelData::setCreator(const QString &value)
{
    creator = value;
}

QString NotemodelData::getName() const
{
    return name;
}

void NotemodelData::setName(const QString &value)
{
    name = value;
}
