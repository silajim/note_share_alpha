#ifndef NOTEMANAGER_H
#define NOTEMANAGER_H

#include "debug.h"

#include <QList>

#include <QThread>

#include "imaphandler.h"
#include "share.h"
#include "dbmanager.h"
#include "settings.h"
#include "smtphandler.h"

class NoteManager : public QObject
{
    Q_OBJECT
public:
    explicit NoteManager(QObject *parent = 0);
    ~NoteManager();


    QString *getUser() const;
    int getMaxid() const;
    bool getLoginState() const;
    void checkUser();

signals: //Synronization beetwen theads.
    void sharesFound(quidlist shares);
    void uidsGot(QList<quint64> uids);
    void messagesGot(QList<Message>);
    void messageParsed(Share);
    void insertShareToDb(Share);
    void getSharefromDb(QString creator , int id);
//Database
    void readSharesFromDb();
    void getmaxIdSignal(QString);

signals: //These are for calling the imap thread;
    void _login(QString user, QString pass , QString server , int port);
    void getHeaders(QList<quint64> uids);
    void getHeaders();
    void _getUids();
    void getmessages(quidlist uids);
    void getAutoconfigFailed();


signals: // Communication beetween ui and NoteManager
    void newShare(Share sh);
    void updateShare(Share sh);
    void gotallShares(sharelist shares);
    void shareGot(Share sh);
    void needLogin();
    void uimessage(QString message);
    void loginstateChanged(bool state);

public slots:
    void findshares(QList<quint64> uids);
    void getUids();
    void getMessages(QList<quint64> uids = QList<quint64>());
    void login (QString user, QString pass, QString server, int port);
    void readShares();
    void getmaxId();
    void loadSharefromDb(QString creator , int id);
    void loginStateChangedSlot(bool state);
    void sendShare(Share sh);
    void exitSlot();

private slots:
    void findSharesPt2(allheaders headers);
    void getMessagesPt2(quidlist uids);
    void getMessagesPt3(messagelist messages);
    void onIspInfo(QList<server*> list);
    void getmaxidslot(int value);


private:
    Dbmanager *data;
    imapHandler *handler;
    QThread imapThread;
    QThread dbthread;
    Settings settings;
    Smtphandler smtp;
    ispdbQuerry *ispdb = NULL;

    QString *user,*pass;
    int maxid;
    bool loginState;
    quint64 uipass = 1213;

    //smtp info

};

#endif // NOTEMANAGER_H
