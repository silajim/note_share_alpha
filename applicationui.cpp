#include "applicationui.h"
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "notemanager.h"

#include "share.h"
#include <QByteArray>
#include "debug.h"

#include <QDataStream>

#include "smtp/SmtpMime"

ApplicationUi::ApplicationUi(QObject *parent) : QObject(parent)
{
    model = new NoteModel();
    readmodel = new ReadShareModel();
    engine = new QQmlApplicationEngine(this);

    engine->rootContext()->setContextProperty("app",this);
    qmlRegisterInterface<NoteModel>("NoteModel");
    qmlRegisterInterface<ReadShareModel>("ReadShareModel");
    qmlRegisterInterface<NotemodelData>("NotemodelData");
    qmlRegisterInterface<userModel>("userModel");
    engine->load(QUrl(QStringLiteral("qrc:/main.qml")));


    manager = new NoteManager(this);

    connect(manager,&NoteManager::needLogin,this,&ApplicationUi::needLogin);
    connect(manager,&NoteManager::gotallShares,this,&ApplicationUi::ongotAllSshares);
    connect(manager,&NoteManager::newShare,this,&ApplicationUi::onNewShare);
    connect(manager,&NoteManager::updateShare,this,&ApplicationUi::onUpdateShare);
    connect(manager,&NoteManager::uimessage,this,&ApplicationUi::uimessage);
    connect(manager,&NoteManager::loginstateChanged,this,&ApplicationUi::loginstateChanged);
    connect(this,&ApplicationUi::exiting,manager,&NoteManager::exitSlot);

    manager->checkUser();

    ///Connet loadshare

    connect(this,&ApplicationUi::loadShareSignal,manager,&NoteManager::loadSharefromDb);
    connect(manager,&NoteManager::shareGot,this,&ApplicationUi::onShareLoaded);


    connect(readmodel,&ReadShareModel::Save,manager,&NoteManager::insertShareToDb);
    connect(readmodel,&ReadShareModel::Save,manager,&NoteManager::sendShare);


    manager->readShares();

}

NoteModel *ApplicationUi::getModel() const
{
    return model;
}

void ApplicationUi::loadShare(QString creator, int id)
{
    qdebug << "Load " << creator << id;
    emit loadShareSignal(creator,id);
}

void ApplicationUi::newShare()
{
    Share tmpSh;
    tmpSh.creator = *manager->getUser();
    tmpSh.id = manager->getMaxid();
    tmpSh.id++;
    readmodel->setShare(tmpSh);
}

ReadShareModel* ApplicationUi::getreadmodel()
{
    return readmodel;
}

void ApplicationUi::login(QString user, QString pass)
{
     manager->login(user,pass,"",0);
}

NoteManager *ApplicationUi::getManager() const
{
    return manager;
}

void ApplicationUi::ongotAllSshares(sharelist shares)
{
    foreach(Share sh , shares){
        model->insertShare(sh);
    }
}

void ApplicationUi::onNewShare(Share sh)
{
    model->insertShare(sh);
}

void ApplicationUi::onUpdateShare(Share sh)
{
    model->updateShare(sh);
}

void ApplicationUi::onShareLoaded(Share sh)
{
    readmodel->setShare(sh);
    emit shareLoaded();
}
