#ifndef MYOUTPUTSTREAMADAPTER_H
#define MYOUTPUTSTREAMADAPTER_H

#include <vmime/vmime.hpp>
#include <QByteArray>

class myoutputStreamAdapter: public vmime::utility::outputStream
{
public:
    myoutputStreamAdapter(size_t &sizeout);
    void flush();
    size_t *size;
    QByteArray getChars() const;

protected:
    void writeImpl(const vmime::byte_t* const data, const size_t count);

private:
     QByteArray chars;
};

#endif // MYOUTPUTSTREAMADAPTER_H
