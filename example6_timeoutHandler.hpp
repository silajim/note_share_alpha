#include <ctime>
#include "vmime/vmime.hpp"
#include <functional>

/** Time out handler.
  * Used to stop the current operation after too much time, or if the user
  * requested cancellation.
  */
class timeoutHandler : public vmime::net::timeoutHandler
{
public:

    timeoutHandler()
        : m_start(time(NULL))
    {
    }


    bool isTimeOut()
    {
        // This is a cancellation point: return true if you want to cancel
        // the current operation. If you return true, handleTimeOut() will
        // be called just after this, and before actually cancelling the
        // operation

        // 10 seconds timeout
        if(_cancel)
            return true;
        return (time(NULL) - m_start) >= 60;  // seconds
    }

    void resetTimeOut()
    {
        // Called at the beginning of an operation (eg. connecting,
        // a read() or a write() on a socket...)
        _cancel = false;
        m_start = time(NULL);
    }
    void cancel(){
        _cancel = true;
    }

    bool handleTimeOut()
    {
        // If isTimeOut() returned true, this function will be called. This
        // allows you to interact with the user, ie. display a prompt to
        // know whether he wants to cancel the operation.

        // If you return false here, the operation will be actually cancelled.
        // If true, the time out is reset and the operation continues.
        return false;
    }

private:
    bool _cancel;
    time_t m_start;
};


class timeoutHandlerFactory : public vmime::net::timeoutHandlerFactory
{
public:

    timeoutHandlerFactory(std::function<void(vmime::shared_ptr <timeoutHandler>)> foo){
        func = foo;
    }

    std::function<void(vmime::shared_ptr <timeoutHandler>)> func;
    vmime::shared_ptr <timeoutHandler> _object;
    vmime::shared_ptr <vmime::net::timeoutHandler> create()
    {
        _object =  vmime::make_shared <timeoutHandler>();
        if(func){
            func(_object);
        }
        return _object; //vmime::make_shared <timeoutHandler>();
    }
};

