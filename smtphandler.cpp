#include "smtphandler.h"
#include <QDataStream>
#include <QCryptographicHash>

Smtphandler::Smtphandler(QObject *parent) : QObject(parent)
{

}

void Smtphandler::sendShare(Share sh)
{
    if(server.isEmpty()){
        emit uimessage("Not loged in, Note will not be shared");
        return;
    }

    SmtpClient smtp(server, port, SmtpClient::TlsConnection);
    smtp.setUser(user);
    smtp.setPassword(pass);

    MimeMessage message(true);

    message.setSender(new EmailAddress(user, ""));
    message.addRecipient(new EmailAddress(sh.creator, ""));
    message.setSubject("NSA test mail");
//    message.getContent().addHeaderLine("X-note-share-alpha-ptx: 1" );
//    message.getContent().addHeaderLine("X-note-share-alpha-ptx: 0" );
//     message.getContent().addHeaderLine("x-note-share-alpha-ptx: 1" );
//      message.getContent().addHeaderLine("x-paparia" );
//    message.getContent().addHeaderLine("X-MS-note-share-alpha-ptx: 1" );
    for(QString e : sh.users){
        message.addCc(new EmailAddress(e,""));
    }

    QByteArray arr ;

    QDataStream stream(&arr,QIODevice::ReadWrite);
    stream << sh;



    MimeAttachment attachment(NULL,NULL);
    attachment.setContent(arr);
    message.addPart(&attachment);

    bool ok;
    ok =smtp.connectToHost();
    if(!ok){
        qdebug << "Failed to connect to host";
        emit mailsent(false);
        smtp.quit();
        return ;
    }
    qdebug << "login";
    if(!smtp.login()){
        emit uimessage("Could not login, wrong username/password");
        qdebug << "Could not login, wrong username/password";
        emit mailsent(false);
        return;
    }
    if(!smtp.sendMail(message)){
        emit uimessage("Could not send Share");
        qdebug << "Could not send Share";
        qdebug << smtp.getResponseText();
        emit mailsent(false);
        smtp.quit();
        return;
    }
    emit mailsent(true);
    smtp.quit();
}

void Smtphandler::setAccount(QString server, int port, QString user, QString pass)
{
    this->server=server;
    this->port = port;
    this->user = user;
    this->pass = pass;
}
