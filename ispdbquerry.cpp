#include "ispdbquerry.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QTemporaryFile>
#include <QDomDocument>
#include <QDomElement>
#include <iostream>
#include <QTcpSocket>
#include <array>
#include <qpair.h>

#include "debug.h"

using namespace std;

Q_DECLARE_METATYPE(QList<server*>)
ispdbQuerry::ispdbQuerry(QObject *parent) : QObject(parent)
{

}

ispdbQuerry::~ispdbQuerry()
{

}

void ispdbQuerry::querry(QString email)
{
    qDebug() << "Querry";
    prov = email.section("@",1,1);
    manager = new QNetworkAccessManager(this);
//    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(result(QNetworkReply*)) );
    connect(manager,&QNetworkAccessManager::finished,this,&ispdbQuerry::result);
    QString address = QString("https://autoconfig.thunderbird.net/v1.1/%1").arg(prov);
    qDebug() << "Address to call";
    qDebug() << address;
    manager->get(QNetworkRequest(QUrl(address)));
}

void ispdbQuerry::result(QNetworkReply *reply)
{

    qdebug << "Result Called!  error " << reply->error();

    if(reply->error()==QNetworkReply::NoError){
        QList<server*> info;
        QList<server*> srlist;
//        qDebug() << "Result called!";
        QDomDocument doc;
        doc.setContent(reply->readAll());

        QDomNode node = doc.firstChild().firstChild().firstChild();


        //    qDebug() << "document Childs " << doc.childNodes().count();
        //    qDebug() << "first node childs " << node.childNodes().count();

        while(!node.isNull()){
            QDomElement element = node.toElement();
            if(!element.isNull() && (element.tagName()=="incomingServer" || element.tagName()=="outgoingServer" )){
//                qDebug() <<"Tagname";
//                cout << qPrintable(element.tagName()) << endl;
                QDomNode n = element.firstChild();
                //            qDebug() << "While childs " << n.childNodes().count();
                if(n.toElement().tagName()=="hostname" && !(n.toElement().text().contains("imap") || n.toElement().text().contains("smtp"))){
                    node = element.nextSibling();
                    continue;
                }
                server *sr = new server();
                while(!n.isNull()){
                    QDomElement e = n.toElement();

//                    qDebug() << e.tagName() << " " << e.text();

                    if(e.tagName() == "hostname" ){
                        sr->setAddress(e.text());

                    }else if(e.tagName() == "port"){
                        sr->setPort(e.text().toInt());
                    }else if(e.tagName() == "authentication"){
//                        qDebug() << "[ISPDB] auth " << e.text();
                        if(e.text()== "OAuth2"){
//                            qDebug() << "[ISPDB] auth 0" ;
                            sr->setAuthentication(server::Auth::OAuth2);

                        }else{
                            if(sr->getAuthentication() != server::Auth::OAuth2)
                                sr->setAuthentication(server::Auth::Standard);
                        }
                    }else if(e.tagName() == "socketType"){

//                        qDebug() << "Adding socket Type " << e.text();
                        sr->setSocketType(e.text());
                    }
                    if( e.nextSibling().isComment()){
                        n = e.nextSibling().nextSibling();
                        continue;
                    }
                    n = e.nextSibling();
                }
                srlist << sr;
            }
            //        node = node.nextSibling();
            if(element.nextSibling().isComment()){
                node = element.nextSibling().nextSibling();
                continue;
            }
            node = element.nextSibling();
        }

        //After we get all the imap and smtp refrences wee need to filter them so we get exactly 1 reference for imap and 1 for smtp;


        std::array<QPair<bool,int>,6> find;
        //Array code:
        // [0] imap_SSL
        // [1] imap_STARTTLS
        // [2] imap other
        // [3] smtp_SSL
        // [4] smtp_STARTTLS
        // [5] smtp other

        for(int i=0;i<6;i++){
            find[i].first = false;
            find[i].second = -1;
        }
        int i=0;
        foreach(server *_server , srlist ){
            int j=0;
            if(_server->getAddress().contains("smtp"))
                j=3;
            if(_server->getSocketType().compare("ssl",Qt::CaseInsensitive)==0){
                find[j+0].first = true;
                find[j+0].second = i;
            } else if(_server->getSocketType().compare("STARTTLS",Qt::CaseInsensitive)==0){
                find[j+1].first = true;
                find[j+1].second = i;
            }else{
                find[j+2].first = true;
                find[j+2].second = i;
            }
            i++;
        }

        if(find[0].first==true){
            info <<  srlist.at(find[0].second);
        }else if(find[1].first==true){
            info <<  srlist.at(find[1].second);
        }

        if(find[3].first==true){
            info <<  srlist.at(find[3].second);
        }else if(find[4].first==true){
            info <<  srlist.at(find[4].second);
        }

        emit information(info);
    }else if(reply->error()==QNetworkReply::ContentNotFoundError){
        qDebug() << "Content not found";
        QTcpSocket *sock = new QTcpSocket();
        connect(sock,SIGNAL(connected()),this,SLOT(imapCheck()));
        connect(sock,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(imapCheckFailed(QAbstractSocket::SocketError)));
        sock->connectToHost("imap."+prov,993);
    }
    reply->deleteLater();
}

void ispdbQuerry::imapCheck()
{
    qDebug() << "Connected";
    QTcpSocket *sock = new QTcpSocket();
    connect(sock,SIGNAL(connected()),this,SLOT(smtpOk()));
    connect(sock,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(smtpFailed(QAbstractSocket::SocketError)));
    sock->connectToHost("smtp."+prov,465);
}

void ispdbQuerry::imapCheckFailed(QAbstractSocket::SocketError error)
{
    qDebug() << "Error " << error;
    QList<server*> info;
    info<< new server();
    info<< new server();
    emit information(info);
}

void ispdbQuerry::smtpOk()
{
    qDebug() << "Smtp check Ok";
    QList<server*> info;
    info<< new server();
    info<< new server();

    info[1]->setAddress("smtp."+prov);
    info[1]->setPort(465);
    info[1]->setAuthentication(server::Auth::Standard);

    emit information(info);
}

void ispdbQuerry::smtpFailed(QAbstractSocket::SocketError error)
{
    qDebug() << "Smtp checl failed";
    QList<server*> info;
    info<< new server();
    info<< new server();
    info[0]->setAddress("imap."+prov);
    info[0]->setPort(993);
    info[0]->setAuthentication(server::Auth::Standard);
    emit information(info);
}

