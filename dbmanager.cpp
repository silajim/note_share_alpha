#include "dbmanager.h"
#include <QFile>
#include <QDir>
#include <QTextStream>

#include <debug.h>

#include<inttypes.h>


Dbmanager::Dbmanager(QObject *parent) : QObject(parent)
{

}

void Dbmanager::init(QString pass)
{
    Q_UNUSED(pass);
    int r;


    qDebug() << "Contacts.db path " << QDir::currentPath()+"/Shares.db";

    r = sqlite3_open((QDir::currentPath()+"/Shares.db").toStdString().c_str(),&db);

    qDebug() << "sqlite open result " << r;
    qDebug("Database Contacts.db opened successfully.");

    QString sql("CREATE TABLE `shares` ( \
                `creator`	TEXT NOT NULL, \
                `id`	INTEGER NOT NULL,\
                `version`	INTEGER,\
                `subject`	TEXT,\
                `note`	TEXT,\
                `users`	BLOB,\
                `todo`	BLOB,\
                PRIMARY KEY(`creator`,`id`)\
                );\
    CREATE TABLE `contacts` (\
                `mail`	TEXT,\
                `name`	TEXT,\
                PRIMARY KEY(`mail`)\
                );");

    r = sqlite3_exec(db,sql.toLatin1().constData(), NULL, NULL, NULL);

    qDebug() << "R result " << r;

    r = sqlite3_exec(db,"PRAGMA foreign_keys = 1;", NULL, NULL, NULL);
}

void Dbmanager::newShare(Share sh)
{
    sqlite3_stmt *statement;
    QByteArray arr ;
    QByteArray todo;
    int r;

    QDataStream stream(&arr,QIODevice::ReadWrite);
    stream << sh.users;
    QDataStream stream2(&todo,QIODevice::ReadWrite);
    stream2 << sh.todo;

    //    qdebug << "QBytr users " << todo.constData();
    qdebug << "Share todo size " << todo.size();


    QString sql ("insert into shares values ('%1' , %2 , %3 , '%4' , '%5', ? , ?)");
    sql = sql.arg(sh.creator).arg(sh.id).arg(sh.version).arg(sh.subject).arg(sh.note);
    if(SQLITE_OK==sqlite3_prepare_v2(db,sql.toUtf8().constData(),-1,&statement,NULL)){
        if(SQLITE_OK==sqlite3_bind_blob(statement,1,arr.constData(),arr.size(),SQLITE_TRANSIENT)){
            if(SQLITE_OK!=sqlite3_bind_blob(statement,2,todo.constData(),todo.size(),SQLITE_TRANSIENT)){
                qdebug << "Bind 2 failed!";
                sqlite3_finalize(statement);
                return;
            }
        }else{
            qdebug << "Bind 1 Failed!";
            sqlite3_finalize(statement);
            return;
        }
        r=sqlite3_step(statement);
    }
    qdebug << "newshare result " << r;
    sqlite3_finalize(statement);
    if(r==SQLITE_DONE){
        emit newShareSaved(sh);
    }else{
        qdebug << sqlite3_errmsg(db);
    }
}

void Dbmanager::updateShare(Share sh)
{
    QByteArray arr ;
    QByteArray todo;
    int r;
    sqlite3_stmt *statement;

    QDataStream stream(&arr,QIODevice::ReadWrite);
    stream << sh.users;
    qdebug << "Update users" << arr;
    QDataStream stream2(&todo,QIODevice::ReadWrite);
    stream2 << sh.todo;
    QString sql ("update shares set creator= '%1' , id= %2 , version= %3 , subject= '%4' , note= '%5', users= ? , todo= ?  where creator = '%6' AND id = %7 ");
    sql = sql.arg(sh.creator).arg(sh.id).arg(sh.version).arg(sh.subject).arg(sh.note).arg(sh.creator).arg(sh.id);
    qdebug << "SQL " << sql;
    if(SQLITE_OK==sqlite3_prepare_v2(db,sql.toUtf8().constData(),-1,&statement,NULL)){
        if(SQLITE_OK==sqlite3_bind_blob(statement,1,arr.constData(),arr.size(),SQLITE_TRANSIENT)){
            if(SQLITE_OK!=sqlite3_bind_blob(statement,2,todo.constData(),todo.size(),SQLITE_TRANSIENT)){
                qdebug << "Bind 2 failed!";
                sqlite3_finalize(statement);
                return;
            }
        }else{
            qdebug << "Bind 1 Failed!";
            sqlite3_finalize(statement);
            return;
        }
        r=sqlite3_step(statement);
        sqlite3_finalize(statement);
    }
    qdebug << "update result " << r;
    if(r==SQLITE_DONE){
        emit updateShareSucceeded(sh);
    }else{
        qdebug << sqlite3_errmsg(db);
    }
}

void Dbmanager::getShare(QString creator, int id)
{

    qdebug << Q_FUNC_INFO;

    sqlite3_stmt *statement;
    int r = 0;

    Share sh;

    QString query = QString("SELECT * FROM shares WHERE creator = '%1' AND id = %2").arg(creator).arg(id);

    qdebug << query;

    r = sqlite3_prepare_v2(db, query.toUtf8().constData(), -1, &statement, 0 );

    if ( r == SQLITE_OK)   {

        r = sqlite3_step(statement);

        if (SQLITE_ROW == r)
        {
            sh.creator = QString::fromUtf8(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
            sh.id = sqlite3_column_int(statement, 1);
            sh.version = sqlite3_column_int(statement, 2);
            sh.subject =  QString::fromUtf8(reinterpret_cast<const char *>(sqlite3_column_text(statement, 3)));
            sh.note =  QString::fromUtf8(reinterpret_cast<const char *>(sqlite3_column_text(statement, 4)));
            QByteArray users = QByteArray::fromRawData(reinterpret_cast<const char*>(sqlite3_column_blob(statement, 5)),sqlite3_column_bytes(statement,5));
            QByteArray todo = QByteArray::fromRawData(reinterpret_cast<const char*>(sqlite3_column_blob(statement, 6)),sqlite3_column_bytes(statement,6));

            QDataStream stream(&users,QIODevice::ReadWrite);
            stream >> sh.users;
            QDataStream stream2(&todo,QIODevice::ReadWrite);
            stream2 >> sh.todo;

            qdebug << "Share fetch " << todo.size();

            emit sharegot(sh);
        }
        else if (SQLITE_DONE == r)
        {
            qdebug << "QUERY END SUCCESS.";

        }
        else if (SQLITE_ERROR == r)
        {
            qdebug << "QUERY END ERROR.";

        }else{
            qdebug << "Querry result other " << r;
        }


    }

    qdebug << "END" << r;

    sqlite3_finalize(statement);
}

void Dbmanager::insertShare(Share sh)
{
    bool exist = false;

    qdebug << "Insert Share " << sh.todo;

    sqlite3_stmt *statement;
    int r = 0;

    QString query = QString("SELECT version FROM shares WHERE creator = '%1' AND id = %2").arg(sh.creator).arg(sh.id);
    qdebug << "Fetch " << query;

    if ( sqlite3_prepare_v2(db, query.toUtf8().constData(), -1, &statement, 0 ) == SQLITE_OK)
    {
        while(1)
        {
            r = sqlite3_step(statement);

            if (SQLITE_ROW == r)
            {
                int version = sqlite3_column_int(statement, 0);
                qdebug << "Version from db " << version;
                qdebug << "SH version" << sh.version;
                if(sh.version<=version){
                    qdebug << "New version is not bigger than the one we have!";
                    return;
                }
                exist = true;
                break;
            }
            else if (SQLITE_DONE == r)
            {
                // qDebug() << "QUERY END SUCCESS.";
                break;
            }
            else if (SQLITE_ERROR == r)
            {
                qDebug() << "QUERY END ERROR.";
                break;
            }
        }
    }

    sqlite3_finalize(statement);

    if(exist)
        updateShare(sh);
    else
        newShare(sh);
}

void Dbmanager::getall()
{
    sqlite3_stmt *statement;
    int r = 0;


    sharelist list;

    QString query = QString("SELECT * FROM shares");
    qDebug() << "Messages being fetched from db...";

    if ( sqlite3_prepare_v2(db, query.toUtf8().constData(), -1, &statement, 0 ) == SQLITE_OK)   {
        while(1){
            qdebug << "Message got";
            r = sqlite3_step(statement);

            if (SQLITE_ROW == r)
            {
                Share sh;
                sh.creator = QString::fromUtf8(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
                sh.id = sqlite3_column_int(statement, 1);
                sh.version = sqlite3_column_int(statement, 2);
                sh.subject =  QString::fromUtf8(reinterpret_cast<const char *>(sqlite3_column_text(statement, 3)));
                sh.note =  QString::fromUtf8(reinterpret_cast<const char *>(sqlite3_column_text(statement, 4)));
                //                QByteArray users = (const char*) sqlite3_column_blob(statement, 6);
                //                QByteArray todo = (const char*) sqlite3_column_blob(statement, 7);

                qdebug << "Subject From Db:" << sh.subject;

                //                QDataStream stream(&users,QIODevice::ReadWrite);
                //                stream >> sh.users;
                //                QDataStream stream2(&todo,QIODevice::ReadWrite);
                //                stream2 >> sh.todo;

                list << sh;
            }
            else if (SQLITE_DONE == r)
            {
                qDebug() << "QUERY END SUCCESS.";
                break;

            }
            else if (SQLITE_ERROR == r)
            {
                qDebug() << "QUERY END ERROR.";
                break;

            }

        }
    }
    emit ongetall(list);

    sqlite3_finalize(statement);
}

void Dbmanager::getmaxId(QString user)
{
    if(user.isEmpty()){
        return;
    }

    sqlite3_stmt *statement;
    int r = 0;
    int id=0;
    QString query = QString("SELECT MAX(id) FROM shares where creator=%1").arg(user);
    if ( sqlite3_prepare_v2(db, query.toUtf8().constData(), -1, &statement, 0 ) == SQLITE_OK)   {
         r = sqlite3_step(statement);
         if (SQLITE_ROW == r)
         {
             id =  sqlite3_column_int(statement, 0);
         }
    }
    emit maxIdGot(id);
}

