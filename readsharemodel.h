#ifndef READSHAREMODEL_H
#define READSHAREMODEL_H

#include <QObject>
#include "share.h"

#include "debug.h"
#include "todomodel.h"
#include "usermodel.h"

class ReadShareModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString Subject READ Subject WRITE setSubject NOTIFY SubjectChanged)
    Q_PROPERTY(QString Note READ Note WRITE setNote NOTIFY NoteChanged)
    Q_PROPERTY(QString Creator READ Creator WRITE setCreator NOTIFY CreatorChanged)
    Q_PROPERTY(userModel* Users READ getUsermodel NOTIFY UsersChanged)

    Q_PROPERTY(quint32 Version READ Version)
    Q_PROPERTY(todoModel* listModel READ listModel NOTIFY ListModelChanged)


public:
    explicit ReadShareModel(QObject *parent = 0);

    Share getShare() const;
    void setShare(Share value);

    quint32 Version();
    int Id();

    QString Subject();
    QString Note();
    QString Creator();
    QStringList Users();
    todoModel* listModel();

    void setSubject(QString &value);
    void setNote(QString &value);
    void setCreator(QString &value);
    void setUsers(QStringList value);


    userModel *getUsermodel();

signals:
    void ListModelChanged();
    void SubjectChanged();
    void NoteChanged();
    void CreatorChanged();
    void UsersChanged();
    void Save(Share);


public slots:
    void save();

private:
    Share sh;
    todoModel todomodel;
    userModel usermodel;

};

#endif // READSHAREMODEL_H
