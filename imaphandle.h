#ifndef IMAPHANDLE_H
#define IMAPHANDLE_H

#include <QObject>
#include <vmime/vmime.hpp>

class imapHandle : public QObject , public vmime::object
{
    Q_OBJECT
public:
    explicit imapHandle(QObject *parent = 0);
    ~imapHandle();


signals:

public slots:

private:
   vmime::shared_ptr <vmime::net::session> g_session;
};

#endif // IMAPHANDLE_H
