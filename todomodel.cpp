#include "todomodel.h"

todoModel::todoModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> todoModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[todoNameRole] = "todoname";
    roles[todoDoneRole] = "tododone";
    return roles;
}

QVariant todoModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
}

bool todoModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}

int todoModel::rowCount(const QModelIndex &parent) const
{
    qdebug << "RowCount " << todo->size();
    return todo->size();
}

QVariant todoModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()){
        qdebug << "INVALID!";
        return QVariant();
    }

    switch (role) {
    case todoNameRole:
        return todo->at(index.row()).first;
        break;
    case todoDoneRole:
        return todo->at(index.row()).second;
    default:
        return QVariant();
    }
}

bool todoModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!

        switch (role) {
        case todoNameRole:
             todo->operator [](index.row()).first = value.toString();
            break;
        case todoDoneRole:
            todo->operator [](index.row()).second = value.toBool();
        default:
            return false;
        }

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags todoModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool todoModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    QPair<QString,bool> tmp;
    todo->operator <<(tmp) ;
    endInsertRows();
}

bool todoModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    todo->removeAt(row);
    endRemoveRows();
}

void todoModel::setTodo(QList<QPair<QString, bool> > *value)
{
    beginResetModel();
    todo = value;
//    qdebug << "Todo set size" << todo->size();
    endResetModel();
}

void todoModel::addTodo()
{
    insertRows(rowCount(),1);
}
