TEMPLATE = app
QT += core
QT += qml quick xml
QT += network
CONFIG += c++11

include(smtp/smtp.pri)
include(Google/Google.pri)

SOURCES += main.cpp \
    imaphandler.cpp \
    message.cpp \
    myoutputstreamadapter.cpp \
    notemanager.cpp \
    share.cpp \
    dbmanager.cpp \
#    smtp/emailaddress.cpp \
#    smtp/mimeattachment.cpp \
#    smtp/mimecontentformatter.cpp \
#    smtp/mimefile.cpp \
#    smtp/mimehtml.cpp \
#    smtp/mimeinlinefile.cpp \
#    smtp/mimemessage.cpp \
#    smtp/mimemultipart.cpp \
#    smtp/mimepart.cpp \
#    smtp/mimetext.cpp \
#    smtp/quotedprintable.cpp \
#    smtp/smtpclient.cpp \
    smtphandler.cpp \
    settings.cpp \
    ispdbquerry.cpp \
    server.cpp \
    applicationui.cpp \
    notemodel.cpp \
    readsharemodel.cpp \
    todomodel.cpp \
    usermodel.cpp \
    simplecrypt.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    imaphandler.h \
    example6_authenticator.hpp \
    example6_certificateVerifier.hpp \
    example6_timeoutHandler.hpp \
    example6_tracer.hpp \
    message.h \
    myoutputstreamadapter.h \
    notemanager.h \
    share.h \
    debug.h \
    dbmanager.h \
#    smtp/emailaddress.h \
#    smtp/mimeattachment.h \
#    smtp/mimecontentformatter.h \
#    smtp/mimefile.h \
#    smtp/mimehtml.h \
#    smtp/mimeinlinefile.h \
#    smtp/mimemessage.h \
#    smtp/mimemultipart.h \
#    smtp/mimepart.h \
#    smtp/mimetext.h \
#    smtp/quotedprintable.h \
#    smtp/smtpclient.h \
#    smtp/smtpexports.h \
#    smtp/SmtpMime \
    smtphandler.h \
    settings.h \
    ispdbquerry.h \
    server.h \
    applicationui.h \
    notemodel.h \
    readsharemodel.h \
    todomodel.h \
    usermodel.h \
    simplecrypt.h

#unix{
#SOURCES+=     smtp/emailaddress.cpp \
#    smtp/mimeattachment.cpp \
#    smtp/mimecontentformatter.cpp \
#    smtp/mimefile.cpp \
#    smtp/mimehtml.cpp \
#    smtp/mimeinlinefile.cpp \
#    smtp/mimemessage.cpp \
#    smtp/mimemultipart.cpp \
#    smtp/mimepart.cpp \
#    smtp/mimetext.cpp \
#    smtp/quotedprintable.cpp \
#    smtp/smtpclient.cpp

#HEADERS+=     smtp/emailaddress.h \
#    smtp/mimeattachment.h \
#    smtp/mimecontentformatter.h \
#    smtp/mimefile.h \
#    smtp/mimehtml.h \
#    smtp/mimeinlinefile.h \
#    smtp/mimemessage.h \
#    smtp/mimemultipart.h \
#    smtp/mimepart.h \
#    smtp/mimetext.h \
#    smtp/quotedprintable.h \
#    smtp/smtpclient.h \
#    smtp/smtpexports.h \
#    smtp/SmtpMime

#}

#qml_scenes.depends = $$PWD/EmailLogin.qml $$PWD/Home.qml $$PWD/ReadShare.qml

qml_scenes.depends = $$PWD/*.qml
qml_scenes.commands =
QMAKE_EXTRA_TARGETS += qml_scenes

win32 {
INCLUDEPATH += include #Vmime includes
    DEPLOY_COMMAND = windeployqt #Check ths further

CONFIG( debug, debug|release ) {
    # debug
    DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/debug/))
} else {
    # release
    DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/release/))
}
warning($${DEPLOY_COMMAND} $${DEPLOY_TARGET})
#QMAKE_POST_LINK += $${DEPLOY_COMMAND} $${DEPLOY_TARGET}
}

win32{
LIBS+= -L$$PWD/libs/win32
LIBS+= -lvmime.dll -lgnutls #-lSMTPEmail
CONFIG( debug, debug|release ) {
QMAKE_POST_LINK += $$QMAKE_COPY $$system_path($$PWD/libs/win32/runtime/) $$system_path($$OUT_PWD/debug/) $$escape_expand(\n\t) $${DEPLOY_COMMAND} --qmldir $$PWD $${DEPLOY_TARGET}  --debug  --no-system-d3d-compiler
}
else{
QMAKE_POST_LINK +=  $$QMAKE_COPY $$system_path($$PWD/libs/win32/runtime/) $$system_path($$OUT_PWD/release/) $$escape_expand(\n\t) $${DEPLOY_COMMAND} --qmldir $$PWD $${DEPLOY_TARGET} --release  --force  --no-system-d3d-compiler
}
}
unix{
LIBS+= $$PWD/libs/linux/libvmime.a -lssl -lcrypto -lgsasl -lanl -lsqlite3 -licuuc

}


LIBS+= -lsqlite3
OTHER_FILES +=  example6.cpp

