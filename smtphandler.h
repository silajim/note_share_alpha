#ifndef SMTPHANDLER_H
#define SMTPHANDLER_H

#include <QObject>
#include <QString>
#include "debug.h"
#include "share.h"
#include "smtp/SmtpMime"

class Smtphandler : public QObject
{
    Q_OBJECT
public:
    explicit Smtphandler(QObject *parent = 0);

signals:
    void uimessage(QString message);
    void mailsent(bool ok);

public slots:
    void sendShare(Share sh );
    void setAccount(QString server, int port , QString user, QString pass);

private:
    QString server;
    QString user;
    QString pass;
    int port;
};

#endif // SMTPHANDLER_H
