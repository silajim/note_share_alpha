/*
 * auth.h
 *
 *  Created on: 05/05/2014
 *      Author: Mij
 */

#ifndef AUTH_H_
#define AUTH_H_

#include <QObject>
#include "Google/oauth/kqoauthmanager.h"
#include "Google/oauth/kqoauth2request.h"
#include "Google/oauth/kqoauthrequest.h"
#include <QUrl>
#include <QSettings>
#include <QDebug>
#include <QMultiMap>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QDateTime>
#include <QStringList>
#include <qtimer.h>


namespace Google{

    class auth: public QObject {
        Q_OBJECT
    public:
        bool hastoken();
        auth(QObject *parent = 0);
        auth(QString ClientID, QString Client_Secret, QObject *parent = 0);
        virtual ~auth();
        void Login();
        QString getToken();
        static auth *instance();

        const QString& getOauthConsumerKey() const {
            return m_oauthConsumerKey;
        }

        void setOauthConsumerKey(const QString& oauthConsumerKey) {
            m_oauthConsumerKey = oauthConsumerKey;
        }

        const QString& getOauthConsumerSecret() const {
            return m_oauthConsumerSecret;
        }

        void setOauthConsumerSecret(const QString& oauthConsumerSecret) {
            m_oauthConsumerSecret = oauthConsumerSecret;
        }

        bool CheckToken();
        void RefreshToken();
        void addOauthscope(QString scope);
        void setSuccesHtml(QString Htmlfile);
        QString ScopestoString();

        const QString& getUserEmail() const
        {
            return userEmail;
        }

        void setUserEmail(const QString& userEmail)
        {
            this->userEmail = userEmail;
        }

        bool isValid() const
        {
            return valid;
        }

        void setValid(bool valid)
        {
            this->valid = valid;
        }

        signals:
        void LogedIn();
        void TokenRefreshed();
        void TokenInvalid();
        void openBrowser(QString url);

    private:
        static auth *m_instance;
        QString m_oauthToken;
        QString m_oauthTokenSecret;
        QString m_oauthConsumerSecret;
        QString m_oauthConsumerKey;
        KQOAuthManager *oauthManager;
        KQOAuth2Request *oauthRequest;
        QSettings oauthSettings;
        QString token;
        QNetworkAccessManager *netmanager;
        bool valid = false;
        bool emitLogin = false;
        QTimer *m_timer = NULL;

        void setTimer(int secs);
        QStringList m_scopes;
        QString userEmail;


        private Q_SLOTS:
        void onTemporaryTokenReceived(QString temporaryToken, QString temporaryTokenSecret);
        void onAuthorizationReceived(QString token, QDateTime expire,QString refresh_token,int rawexpire);
        void onAccessTokenReceived(QString token, QString tokenSecret);
        void onAuthorizedRequestDone();
        void onRequestReady(QByteArray);
        void onTokenRefreshed(QNetworkReply *reply);
        void onTokenNotValid();
        //	void onOpenBrowserRecived(QString url);
    };
}
#endif /* AUTH_H_ */
