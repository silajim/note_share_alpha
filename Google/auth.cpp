/*
 * auth.cpp
 *
 *  Created on: 05/05/2014
 *      Author: Mij
 */

#include "auth.h"
#include <Qt>
#include <QDate>
#include <QUrlQuery>
#include <QSslConfiguration>
namespace Google {

    auth* auth::m_instance= 0;

    auth::auth(QObject *parent) :
		        QObject(parent) {
        // TODO Auto-generated constructor stub
        oauthManager = new KQOAuthManager(this);
        oauthRequest = new KQOAuth2Request;
        oauthManager->setSuccessHtmlFile("assets:///loginSuccess.html");
        oauthRequest->setEnableDebugOutput(true);
        token = oauthSettings.value("GooglePlus/oauth_token").toString();
    }
    auth::auth(QString ClientID, QString Client_Secret, QObject *parent) :
		        QObject(parent) {
        qDebug()<<"1";
        qDebug()<<"2";
        oauthManager = new KQOAuthManager(this);
        oauthRequest = new KQOAuth2Request;
//        oauthManager->setSuccessHtmlFile("assets:///loginSuccess.html");

        oauthRequest->setEnableDebugOutput(true);

        m_oauthConsumerKey = ClientID;
        m_oauthConsumerSecret = Client_Secret;

        m_instance = this;
    }

    auth* auth::instance(){
        if(m_instance==0){
            qDebug() << "5";
            m_instance = new auth;
        }
        return m_instance;
    }

    QString auth::getToken(){
        return token;
    }
    bool auth::hastoken() {
        bool res = true;
        if (oauthSettings.value("GooglePlus/oauth_token").toString().isEmpty()) {
            res = false;

        }
        return res;
    }

    void auth::Login() {
        if (!hastoken()) {
            connect(oauthManager, SIGNAL(temporaryTokenReceived(QString, QString)),this, SLOT(onTemporaryTokenReceived(QString, QString)));

            connect(oauthManager, SIGNAL(authorizationReceived(QString,QDateTime,QString,int)),this, SLOT(onAuthorizationReceived(QString , QDateTime ,QString, int)));

            connect(oauthManager, SIGNAL(accessTokenReceived(QString, QString)),this, SLOT(onAccessTokenReceived(QString, QString)));

            connect(oauthManager, SIGNAL(requestReady(QByteArray)), this,SLOT(onRequestReady(QByteArray)));

            connect(oauthManager,SIGNAL(OpenBrowser(QString)),this,SIGNAL(openBrowser(QString)));

            oauthManager->setHandleUserAuthorization(true);
            KQOAuthParameters params;
//            QString scope ="https://www.googleapis.com/auth/plus.me+https://www.googleapis.com/auth/plus.media.upload+https://www.googleapis.com/auth/plus.stream.write+https://www.googleapis.com/auth/plus.profiles.read+https://www.googleapis.com/auth/drive+https://www.googleapis.com/auth/drive.file";
            if(!userEmail.isEmpty()){
               addOauthscope("&login_hint="+userEmail);
            }
            params.insert("scope", ScopestoString());
            oauthManager->setClientId(m_oauthConsumerKey);
            oauthManager->setClientIdSecret(m_oauthConsumerSecret);
            oauthManager->getOauth2UserAuthorization(QUrl("https://accounts.google.com/o/oauth2/auth"), m_oauthConsumerKey, params);
        }else{
            qDebug() << "Token Validity " << CheckToken();
            if(!CheckToken()){
                RefreshToken();
//                emit LogedIn();
            }else{
                token = oauthSettings.value("GooglePlus/oauth_token").toString();
                emit LogedIn();
            }
        }
    }
    void auth::onTemporaryTokenReceived(QString token, QString tokenSecret) {
        qDebug() << "Temporary token received: " << token << tokenSecret;

        QUrl userAuthURL("https://accounts.google.com/o/oauth2/auth");

        if (oauthManager->lastError() == KQOAuthManager::NoError) {
            qDebug() << "Asking for user's permission to access protected resources. Opening URL: " << userAuthURL;
            oauthManager->getUserAuthorization(userAuthURL);
        }
    }
    void auth::onAuthorizationReceived(QString token, QDateTime expire,QString refresh_token, int rawexpire) {
        qDebug() << "User authorization received: " << token;

        if (token == NULL) {
            return;
        }
        if (oauthManager->lastError() != KQOAuthManager::NoError) {
        }
        this->token = token;
        oauthSettings.setValue("GooglePlus/oauth_token", token);
        oauthSettings.setValue("GooglePlus/refresh",refresh_token);
        oauthSettings.setValue("GooglePlus/expire",expire.toString(Qt::ISODate));
        oauthSettings.sync();

        qDebug() << "Access tokens now stored.";
        setTimer(rawexpire);
        emit LogedIn();
        disconnect(oauthManager, SIGNAL(authorizationReceived(QString,QString)),this, SLOT(onAuthorizationReceived(QString , QDateTime ,QString,int)));
    }

    void auth::onAccessTokenReceived(QString token, QString tokenSecret) {
        qDebug() << "Access token received: " << token << tokenSecret;

        oauthSettings.setValue("GooglePlus/oauth_token", token);
        oauthSettings.setValue("GooglePlus/oauth_token_secret", tokenSecret);

        qDebug()<< "Access tokens now stored. You are ready to send Tweets from user's account!";
        oauthSettings.sync();

        emit LogedIn();
    }

    void auth::onAuthorizedRequestDone() {
        qDebug() << "Request sent to Twitter!";
    }

    bool auth::CheckToken() {
        QDateTime now;
        now	= QDateTime::currentDateTime().toUTC();
        QDateTime then = QDateTime::fromString(oauthSettings.value("GooglePlus/expire").toString(),Qt::ISODate);
        qDebug() << "now " << now.toString();
        qDebug() << "Then " << then.toString();
        then.toUTC();
        now.addSecs(-30);
        if(then<now){
            return false;
        }else
            return true;
    }

    void auth::RefreshToken() {
        qDebug() << "Refreshing token";
        netmanager = new QNetworkAccessManager(this);

        QUrlQuery url;
        url.addQueryItem("client_secret",m_oauthConsumerSecret);
        url.addQueryItem("refresh_token",oauthSettings.value("GooglePlus/refresh").toString());
        url.addQueryItem("grant_type","refresh_token");
        url.addQueryItem("client_id",m_oauthConsumerKey);
        QNetworkRequest req(QUrl("https://accounts.google.com/o/oauth2/token"));       
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        //    req.setRawHeader("Host", "www.googleapis.com");
        req.setHeader(QNetworkRequest::ContentLengthHeader, url.query().toUtf8().toPercentEncoding().size());

        qDebug() << "Headers " << req.rawHeaderList() ;
        connect(netmanager, SIGNAL(finished(QNetworkReply*)), this,  SLOT(onTokenRefreshed(QNetworkReply*)));
        qDebug() << "Data to send " << url.query().toUtf8().toPercentEncoding();
        netmanager->post(req,url.query().toUtf8());

    }

    void auth::onRequestReady(QByteArray response) {
        qDebug() << "Response from the service: " << response;
    }
    auth::~auth() {
        // TODO Auto-generated destructor stub
    }

    void auth::setTimer(int secs){
        if(m_timer == NULL){
            m_timer = new QTimer();
            m_timer->setSingleShot(true);
            connect(m_timer,SIGNAL(timeout()),this,SLOT(onTokenNotValid()));
        }
        m_timer->start(secs-30);
    }

    void auth::onTokenRefreshed(QNetworkReply* reply)
    {
        disconnect(sender(), SIGNAL(finished(QNetworkReply*)), this, SLOT(onTokenRefreshed(QNetworkReply*)));
        qDebug() << "Got Reply";
        if (reply->error() == QNetworkReply::NoError) {
            qDebug() << "Oauth Reply";
            qDebug() << "Reply Bytes" << reply->bytesAvailable();
            QByteArray rep = reply->readAll();
            QString _str(rep);
            _str.remove("{");
            _str.remove("}");
            _str.remove('"');
            _str.remove("\n");
            _str.remove(" ");
            QRegExp reg("[:,]");
            QMultiMap<QString, QString> values;
            QStringList strl = _str.split(reg, QString::SkipEmptyParts);
            for (int i = 0; i < strl.length(); i = i + 2) {
                values.insert(strl.at(i), strl.at(i + 1));
            }

            QDateTime time;
            time = QDateTime::currentDateTime().toUTC();
            time = time.addSecs(values.value("expires_in").toInt());
            //qDebug() << "Yparxei?" << rep.contains("refresh_token");
            if(strl.length()>=3 && values.contains("access_token") && values.contains("token_type")){
                oauthSettings.setValue("GooglePlus/oauth_token", values.value("access_token"));
                oauthSettings.setValue("GooglePlus/expire",time.toString(Qt::ISODate));
                oauthSettings.sync();
                token =  values.value("access_token");
                setTimer(values.value("expires_in").toInt());
            }
//            if(emitLogin){
//                emit LogedIn();
//                emitLogin =false;
//            }else{
//                emit TokenRefreshed();
//            }
            emit LogedIn();
            qDebug() << "Token refreshed";
            //        emit authorizationReceived(values.value("access_token"), time,values.value("refresh_token"));
        }else{
            qDebug() << "Error " << reply->error() << "bytes " << reply->bytesAvailable() << " " << reply->readAll();
        }
    }

    QString auth::ScopestoString()
    {
        QString sc;
        int i =0;
        for(i=0;i<m_scopes.length()-1;i++){
            sc.append(m_scopes.at(i)+"+");
        }
        sc.append(m_scopes.at(i));
        return sc;

    }

    void auth::onTokenNotValid()
    {
        valid = false;
        emit TokenInvalid();
    }
    void auth::addOauthscope(QString scope)
    {
        m_scopes << scope;
    }
    void auth::setSuccesHtml(QString Htmlfile)
    {
        oauthManager->setSuccessHtmlFile(Htmlfile);
    }
}
