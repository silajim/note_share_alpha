#ifndef KQOAUTH2REQUEST_P_H
#define KQOAUTH2REQUEST_P_H

#include "kqoauthglobals.h"

class KQOAuthRequest;
class KQOAuth2Request_Private
{
public:
    KQOAuth2Request_Private();
    ~KQOAuth2Request_Private();
};

#endif // KQOAUTHREQUEST_XAUTH_P_H
