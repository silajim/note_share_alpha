APP_NAME = Google

#CONFIG += qt warn_on cascades10

QMAKE_CXXFLAGS += -Wc,-std=gnu++0x

QT += core

QT -= gui

TEMPLATE= lib

CONFIG += staticlib

#LIBS+= -lQjson

device {
    QMAKE_CC = qcc -V4.8.3,gcc_ntoarmv7le  
    QMAKE_CXX = qcc -V4.8.3,gcc_ntoarmv7le 
    QMAKE_LINK = qcc -V4.8.3,gcc_ntoarmv7le
}
simulator {
    QMAKE_CC = qcc -V4.8.3,gcc_ntox86  
    QMAKE_CXX = qcc -V4.8.3,gcc_ntox86  
    QMAKE_LINK = qcc -V4.8.3,gcc_ntox86
}
include(config.pri)
