QT       += core network
INCLUDEPATH += $$PWD $$PWD/oauth
DEPENDPATH += $$PWD
CONFIG += c++11

SOURCES += $$PWD/*.c* $$PWD/oauth/*.c*
HEADERS += $$PWD/*.h* $$PWD/oauth/*.h*
