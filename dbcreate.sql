CREATE TABLE `shares` (
	`creator`	TEXT NOT NULL,
	`id`	INTEGER NOT NULL,
	`version`	INTEGER,
	`subject`	TEXT,
	`note`	TEXT,
	`users`	BLOB,
	`todo`	BLOB,
	PRIMARY KEY(`creator`,`id`)
);

CREATE TABLE `contacts` (
	`mail`	TEXT,
	`name`	TEXT,
	PRIMARY KEY(`mail`)
);