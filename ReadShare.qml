import QtQuick 2.5
import QtQuick.Controls 2.0

Rectangle {
    anchors.fill: parent
    signal close();
    MouseArea{
        anchors.fill: parent

    }

    Item {
        id: header
        height: 40
        anchors.left: parent.left
        anchors.right: parent.right
        Separator{
            anchors.bottom: parent.bottom
        }
        Button{
            height: parent.height - 10
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            text: "Cancel"
            onClicked: close();
        }
        Button{
            height: parent.height - 10
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            text: "Save"
            onClicked: {
                app.getreadmodel().save();
                close();
            }
        }
    }

    Item{
        id:cre_item
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: header.bottom
        anchors.margins: 5
        height: creator.height + expand.height + 5
        z: parent.z+1
        Label{
            id: creator_label
            text: "Creator :"
//            anchors.verticalCenter: parent.verticalCenter
            anchors.top: parent.top
            anchors.topMargin: 5

        }
        TextArea {
            id: creator
            anchors.verticalCenter: creator_label.verticalCenter
            anchors.left: creator_label.right
            anchors.leftMargin: 5
            text: app.getreadmodel().Creator;
        }
        Rectangle{
            id: expand
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: creator_label.bottom
            anchors.topMargin: 3
            color: "transparent"
            border.width: 1
            border.color: "lightgrey"
            height: expbutton.checked ? 170 : 30

            Button{
                id: expbutton;
                checkable: true
                anchors.bottom: parent.bottom;
                anchors.right: parent.right
                anchors.left: parent.left
                height: 30
                text: checked ? "Collapse" : "Expand"

            }
          ListView{
              id: userslist;
                anchors.top: parent.top
                anchors.bottom: expbutton.top
                anchors.left: parent.left
                anchors.right: parent.right
                clip: true

                model: app.getreadmodel().Users //["salamij92@hotmail.com","papa@skata.com"]//app.getreadmodel().Users
//                visible: true
                header: Button {
                    height: 30
                    anchors.left: parent.left
                    anchors.right: parent.right
                    text: "Add"

                    onClicked: userslist.model.insert();
                }
                onHeightChanged: console.log("Height " + height)


                delegate: Item{
                    height: 30
                    anchors.left: parent.left
                    anchors.right: parent.right
                    TextField{
                        height: parent.height
                        anchors.top: parent.top
                         anchors.left: parent.left
                        anchors.right: parent.right
                        text: display
                        onTextChanged: display = text
                    }
                    Separator{
                        anchors.bottom: parent.bottom
                    }
                }
            }
        }

        Separator{
            anchors.bottom: parent.bottom
        }
    }
    Label{
        id:version
        anchors.right: parent.right;
        anchors.rightMargin: 5
        anchors.verticalCenter: parent.verticalCenter
        text: "Version " + app.getreadmodel().Version
    }


    Rectangle{
        anchors.top: cre_item.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        anchors.right: parent.right
        anchors.bottom: footer.top

        TextField{
            id: subj
            anchors{
                top: parent.top
                left: parent.left
                right: parent.right
            }

            font.bold: true
            font.pointSize: 13
            text:  app.getreadmodel().Subject
            placeholderText: "Note name"
            Binding{
                target: app.getreadmodel();
                property: "Subject"
                value: subj.text
            }

        }

        Flickable{
            id: flickable
            focus : true
            clip: true
            anchors.top: subj.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            contentHeight:  note.height+todo_col.height
            onContentHeightChanged: {
                console.log("Content height, changed " + contentHeight)
            }

            anchors{
                top: subj.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            ScrollBar.vertical: ScrollBar {
                parent: flickable.parent
//                anchors.top: flickable.top
                anchors.right: flickable.right
//                anchors.bottom: flickable.bottom
                active: true
            }
            TextArea{
                id: note
                anchors{
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                font.bold: true
                font.pointSize: 10
                text:  app.getreadmodel().Note
                placeholderText: "Note"

                Binding{
                    target: app.getreadmodel();
                    property: "Note"
                    value: note.text
                }

                Separator{
                    anchors.bottom: parent.bottom
                }

            }
            Column{
                id:todo_col
                anchors{
                    top: note.bottom
                    topMargin: 5
                    left: parent.left
                    right: parent.right
                }

                Repeater{
                    id:todo
                    model: app.getreadmodel().listModel;
                    anchors.left: parent.left
                    anchors.right: parent.right

                    delegate: Item {
                        height: textf.height + 10
                        anchors.left: parent.left
                        anchors.right: parent.right
                        TextField{
                            id: textf
                            anchors{
                                left:parent.left
                                right: check.left
                            }
                            text: todoname
                            onTextChanged: todoname = text

                            Component.onCompleted: {
                                console.log(textf.text)
                            }

                        }
                        CheckBox{
                            id:check
                            anchors{
                                right: parent.right
                            }
                            checked: tododone
                            onCheckedChanged: tododone = checked
                        }
                    }
                }
            }
        }
    }
    Rectangle{
        id: footer;
        height: 50
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        Separator{
            anchors.top: parent.top
        }
        Button{
            anchors.centerIn: parent
            text: "Add To-do"
            onClicked: app.getreadmodel().listModel.addTodo();
        }
    }
}

