import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

Item{
    height: parent.height
    width: parent.width

    onWidthChanged: console.log("Width Changed " + width);
    onHeightChanged:  console.log("Hewight Changed " + height)

    //    Text {
    //        id: tpt
    //        text: qsTr("text")
    //    }
    Component{
        id: read
        ReadShare{
            onClose:{
//                loader2.enabled = false
                loader2.sourceComponent=undefined;
            }

        }
    }
    ListView{

        model: app.getModel();
        clip: true
        anchors{
            top: parent.top;
            topMargin: 10
            left: parent.left;
            leftMargin: 5
            right : parent.right
            rightMargin: 5
            bottom: bottomBar.top
            //            fill: parent
        }
        delegate: Item{
            id: delegatemain
            height: idname.height + idcreator.height + 10
            width: parent.width
            Label{
                id: idname;
                text: name;
                font.pointSize: 12
                anchors.top: parent.top
                anchors.left: parent.left
            }
            Text {
                id: idcreator
                text: creator;

                anchors.top: idname.bottom
                anchors.topMargin: 6
                anchors.left: parent.left
            }

            Text {
                id: idversion
                text: version
                anchors.top: idname.bottom
                anchors.topMargin: 5
                anchors.right : parent.right
            }

            Separator{
                anchors.bottom: parent.bottom

                Component.onCompleted: console.log("Loaded!")
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    //                    loader.setSource(read);
                    app.loadShare(creator,id)
                }
            }

        }

    }
    Connections{
        target: app;
        onShareLoaded:{
            console.log("Load read qml")
            loader2.sourceComponent = read
        }
    }
    Item{
        id: bottomBar
        height: col.height +20
        anchors{
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        Separator{
            anchors.top: parent.top
        }

        Item{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: col.width
            height: col.height
            Button{
                id: col
                text: "New Note"
                onClicked: {
                    app.newShare();
                    loader2.sourceComponent = read
                }
            }
        }
    }
}


