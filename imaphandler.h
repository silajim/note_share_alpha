#ifndef IMAPHANDLER_H
#define IMAPHANDLER_H

#include <QObject>
#include <QString>
#include <iostream>


#include <vmime/vmime.hpp>
#include <vmime/net/imap/imap.hpp>
#include<vmime/net/imap/IMAPConnection.hpp>
#include <vmime/platforms/posix/posixHandler.hpp>

#include "example6_timeoutHandler.hpp"

#include <vmime/message.hpp>
#include <vmime/net/imap/IMAPParser.hpp>


#include <vmime/message.hpp>

#include <functional>

#include <QHash>
#include <vector>

#include "message.h"
#include "ispdbquerry.h"

#include <QtConcurrent/QtConcurrent>
#include <QThreadPool>

#include <QThread>

#include <QTimer>




typedef QMap<quint64,QStringList> allheaders;
typedef QList<quint64> quidlist;
typedef QList<Message> messagelist;

//#include "example6_tracer.hpp"
//#include "example6_authenticator.hpp"
//#include "example6_certificateVerifier.hpp"
//#include "example6_timeoutHandler.hpp"

using namespace std;

class imapHandler : public QObject
{
    Q_OBJECT
public:
    explicit imapHandler(QObject *parent = 0);
    ~imapHandler();

    quint64 getMaxuid() const;
    void setMaxuid(const quint64 value);
    void cancelOpration();
    void timeoutHCreated(shared_ptr<timeoutHandler> _th);

signals:
    void newmails(QList<quint64> newuid);
    void loginStateChanged(bool state);
    void headersGot(allheaders headers);
    void mailsGot(messagelist);
    void uidGot(QList<quint64> uids);
    void uimessage(QString message);


public slots:
    void login(QString email, QString passw , QString server , int port);
    QList<quint64> getUids(int start=1, int end=-1);
    const QMap<quint64, QStringList> getHeaders( QList<quint64> uids);
    const QMap<quint64, QStringList> getHeaders();
    QList<Message> getMails(quidlist uids);
    //Extend Vmime via Sockets
    void SearchForHeaders(QString header="X-note-share-alpha-ptx");

private:
vmime::shared_ptr <vmime::net::session> g_session;
vmime::shared_ptr <vmime::net::store> st;
Message dogetMails(const quint64 uid);
vmime::shared_ptr <vmime::net::folder> f;
shared_ptr <vmime::net::timeoutHandlerFactory> thf;
shared_ptr <timeoutHandler> th;
QTimer *timer = 0;
quint64 getmax(QList<quint64> uids);
quint64 maxuid = 1;
size_t totalmsgs;
bool logintoStore(vmime::shared_ptr <vmime::net::store> &st);
bool uidsignalEnable = true;
server serv;

int indexofmethod; //we need the index of the method running before entering the Oauth2 flow, in order to return later and continue the procedure.
QGenericArgument arguments[9]; //Store the argouments.




//Login details
QString email;
QString password;
QString _server;
int port;


private slots:
void checkfornewmails();

};

#endif // IMAPHANDLER_H
