#include "notemanager.h"
#include "simplecrypt.h"

#include <QMetaObject>

NoteManager::NoteManager(QObject *parent) : QObject(parent)
{

    handler = new imapHandler();
    handler->setMaxuid(settings.getMaxUid());
    data = new Dbmanager();
    data->init();
    user = new QString(settings.getUserName());
    connect(this, SIGNAL(_login(QString,QString,QString,int)),handler,SLOT(login(QString,QString,QString,int)));
    connect(this, SIGNAL(getHeaders(QList<quint64>)),handler,SLOT(getHeaders(QList<quint64>)));
    connect(this, SIGNAL(getHeaders()),handler,SLOT(getHeaders()));
    connect(this, SIGNAL(_getUids()),handler,SLOT(getUids()));
    connect(this,SIGNAL(getmessages(quidlist)),handler,SLOT(getMails(quidlist)));

    connect(handler,SIGNAL(headersGot(allheaders)),this,SLOT(findSharesPt2(allheaders)));
    connect(this,SIGNAL(insertShareToDb(Share)),data,SLOT(insertShare(Share)));
    connect(handler, SIGNAL(mailsGot(messagelist)),this,SLOT(getMessagesPt3(messagelist)));
    connect(this,SIGNAL(sharesFound(quidlist)),this,SLOT(getMessagesPt2(quidlist)));
    connect(handler, &imapHandler::uimessage,this,&NoteManager::uimessage);
    connect(&smtp,&Smtphandler::uimessage,this,&NoteManager::uimessage);


    connect(this,&NoteManager::getSharefromDb,data,&Dbmanager::getShare);
    connect(data,&Dbmanager::maxIdGot,this,&NoteManager::getmaxidslot);
    connect(handler,&imapHandler::loginStateChanged,this,&NoteManager::loginStateChangedSlot);
    connect(handler,&imapHandler::loginStateChanged,this,&NoteManager::loginstateChanged);


    //Re emit signals to be handled by ui.
    connect(this,&NoteManager::readSharesFromDb,data,&Dbmanager::getall);
    connect(data,&Dbmanager::ongetall,this,&NoteManager::gotallShares);
    connect(data,&Dbmanager::newShareSaved,this,&NoteManager::newShare);
    connect(data,&Dbmanager::updateShareSucceeded,this,&NoteManager::updateShare);
    connect(data,&Dbmanager::sharegot,this,&NoteManager::shareGot);

    handler->moveToThread(&imapThread);
    imapThread.start();
    data->moveToThread(&dbthread);
    dbthread.start();
    getmaxId();
    user = new QString(settings.getUserName());
    SimpleCrypt crypt(uipass);
    pass = new QString (crypt.decryptToString(settings.getPassword()));

    //    readShares();
    login(*user,*pass,"",0);
}

NoteManager::~NoteManager()
{
    qdebug << "Note Manager destructor";
    handler->cancelOpration();
    imapThread.quit();
    if(!imapThread.wait(6000)){
        imapThread.terminate();
        imapThread.wait(1000*10);
    }else{
        qdebug << "imapthread terminated";
        delete handler;
    }
    dbthread.quit();
    dbthread.wait(6000);
    delete data;

}

void NoteManager::login(QString user, QString pass,QString server, int port)
{

    qdebug << "User " << user;
    qdebug << "pass" << pass;
    if(user.isEmpty()){
        return;
    }
    if(server.isEmpty() || port==0){
        ispdb = new ispdbQuerry(this);
        connect(ispdb,SIGNAL(information(QList<server*>)),this,SLOT(onIspInfo(QList<server*>)));
        this->user = new QString(user);
        this->pass = new QString(pass);
        ispdb->querry(user);
        return;
    }
    emit _login(user,pass,server,port);
}

void NoteManager::readShares()
{
    emit readSharesFromDb();
}

void NoteManager::getmaxId()
{
    emit getmaxIdSignal(*user);
}

void NoteManager::loadSharefromDb(QString creator, int id)
{
    qdebug << Q_FUNC_INFO;
    emit getSharefromDb(creator, id);
}

void NoteManager::loginStateChangedSlot(bool state)
{
    loginState = state;
    if(state){
        settings.setUserName(*user);
        getmaxId();
        SimpleCrypt cryp(uipass);
        settings.setPassword(cryp.encryptToString(*pass));
    }
}

void NoteManager::sendShare(Share sh)
{
    smtp.sendShare(sh);
}

void NoteManager::exitSlot()
{
    qdebug << "Exit slot";
}

void NoteManager::findshares(QList<quint64> uids)
{
    disconnect(handler,SIGNAL(uidGot(QList<quint64>)),0,0);
    QObject::connect(handler,SIGNAL(headersGot(QMap<quint64,QStringList>)),this,SLOT(findSharesPt2(QMap<quint64,QStringList>)));
    emit getHeaders(uids);
    return;
}

void NoteManager::getUids()
{
    QObject::connect(handler,SIGNAL(uidGot(QList<quint64>)),this,SIGNAL(uidsGot(QList<quint64>)));
    //    QMetaObject::invokeMethod(handler,"getUids");
    emit _getUids();
    return;
}

void NoteManager::getMessages(QList<quint64> uids)
{
    if(uids.isEmpty()){ //in case it's empty it means that the user requested a manual refresh
        QObject::connect(handler,SIGNAL(headersGot(QMap<quint64,QStringList>)),this,SLOT(findSharesPt2(QMap<quint64,QStringList>)));


        emit getHeaders();
    }else{
        getMessagesPt2(uids);
    }
}

void NoteManager::findSharesPt2(allheaders headers)
{
    qdebug << "findSharesPt2";
    qdebug << "header list " << headers.size();

    //    disconnect(handler,SIGNAL(headersGot(QMap<quint64,QStringList>)),0,0);
    QList<quint64> shares;
    //    QMap<quint64,QStringList> headers = handler->getHeaders(uids);
    for(auto key : headers.keys()){
        for(QString header : headers.value(key)){
            //            qdebug << "header " << header;

            if(header == "X-note-share-alpha-ptx: 1"){
                qdebug << "Share found! " << key;
                shares << key;
                break;
            }
        }
    }
    emit sharesFound(shares);
    return;
}

void NoteManager::getMessagesPt2(quidlist uids)
{
    qdebug << "getMessagesPt2";
    qdebug << "uid size " << uids.size();
    emit getmessages(uids);
}

void NoteManager::getMessagesPt3(messagelist messages)
{
    for(auto e:messages){
        if(e.changelog.size()==0){
            continue;
        }
        Share sh;
        QDataStream stream (&e.changelog[0],QIODevice::ReadOnly);
        stream >> sh;
        if(e.getRecipient()!=sh.creator)
            continue;
        QByteArray todo;
//        QDataStream stream2(&todo,QIODevice::ReadWrite);
//        stream2 << sh.todo;

        emit insertShareToDb(sh);
    }
    settings.setMaxUid(handler->getMaxuid());
    getmaxId();
}

void NoteManager::onIspInfo(QList<server *> list)
{
    if(list.size()<2){
        emit uimessage("Please enter Email provider information manually!");
        emit getAutoconfigFailed();
        return;
    }
    login(*user,*pass,list[0]->getAddress(),list[0]->getPort());
    smtp.setAccount(list[1]->getAddress(),list[1]->getPort(),*user,*pass);
}

void NoteManager::getmaxidslot(int value)
{
    maxid=value;
}


bool NoteManager::getLoginState() const
{
    return loginState;
}

void NoteManager::checkUser()
{
    if(user->isEmpty()){
        emit needLogin();
    }
}

int NoteManager::getMaxid() const
{
    return maxid;
}

QString *NoteManager::getUser() const
{
    return user;
}

