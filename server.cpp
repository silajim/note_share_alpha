#include "server.h"

server::server()
{

}

server::~server()
{

}
QString server::getEmail_provider() const
{
    return email_provider;
}

void server::setEmail_provider(const QString &value)
{
    email_provider = value;
}
QString server::getAddress() const
{
    return address;
}

void server::setAddress(const QString &value)
{
    address = value;
}
int server::getPort() const
{
    return port;
}

void server::setPort(const int &value)
{
    port = value;
}
server::Auth server::getAuthentication() const
{
    return authentication;
}

void server::setAuthentication(const Auth &value)
{
    authentication = value;
}
QString server::getSocketType() const
{
    return socketType;
}

void server::setSocketType(const QString &value)
{
    socketType = value;
}






