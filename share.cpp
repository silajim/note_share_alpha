#include "share.h"
#include "debug.h"
Share::Share()
{
    qRegisterMetaType<sharelist>("sharelist");
    qRegisterMetaType<Share>("Share");

}

QDataStream &operator <<(QDataStream &out , Share &sh)
{
 out << sh.id <<sh.creator << sh.version << sh.subject << sh.note << sh.todo << sh.users;
 qdebug << "---------OUT--------";
 qdebug << "id " <<  sh.id;
 qdebug << "Creator" << sh.creator;
 return out;
}

QDataStream &operator >>(QDataStream &in, Share &sh)
{
    in >> sh.id >> sh.creator >> sh.version >> sh.subject >> sh.note >> sh.todo >> sh.users;
    qdebug << "---------IN--------";
    qdebug << "id " <<  sh.id;
    qdebug << "Creator" << sh.creator;
    qdebug << "Version" << sh.version;
    qdebug << "Subject" << sh.subject;
    qdebug << "Note" << sh.note;
    qdebug << "todo" << sh.todo;
    qdebug << "Users" << sh.users;
    qdebug << "-------------------";
    return in;
}

