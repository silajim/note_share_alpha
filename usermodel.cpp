#include "usermodel.h"
#include "debug.h"

userModel::userModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int userModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.

    qdebug << "RowCount " << userlist->size();
    return userlist->size();
    // FIXME: Implement me!
}

QVariant userModel::data(const QModelIndex &index, int role) const
{

    if(role==Qt::DisplayRole){
        return userlist->at(index.row());
    }

    // FIXME: Implement me!
    return QVariant();
}

bool userModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        qdebug << "user " << value.toString();
        userlist->operator [](index.row()) = value.toString();
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags userModel::flags(const QModelIndex &index) const
{
//    if (!index.isValid())
//        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool userModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    userlist->operator <<( "");
    endInsertRows();
}

bool userModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
}

void userModel::setUserlist(QStringList *value)
{
    beginResetModel();
    userlist = value;
    endResetModel();
}

userModel::insert()
{
    insertRows(rowCount(),1);
}
