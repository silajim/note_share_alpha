#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include <sqlite3.h>
#include "share.h"
#include <QString>

class Dbmanager : public QObject
{
    Q_OBJECT
public:
    explicit Dbmanager(QObject *parent = 0);

signals:

public slots:
    void init(QString pass = "");
    void newShare(Share sh);
    void updateShare(Share sh);
    void getShare(QString creator , int id);
    void insertShare(Share sh);
    void getall();
    void getmaxId(QString user);
signals:
    void newShareSaved(Share sh);
    void updateShareSucceeded(Share sh);
    void sharegot(Share sh);
    void newShareError(Share sh, QString ecode, int code =0);
    void updateShareError(Share sh, QString ecode , int code =0);
    void sharegotError(QString creator , int id, QString ecode, int code =0);
    void ongetall(sharelist);
    void maxIdGot(int id);


private:
     sqlite3 *db;
};

#endif // DBMANAGER_H
