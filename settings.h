#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include "debug.h"
#include <QSettings>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);
    quint64 getMaxUid();
    void setMaxUid(quint64 uid);

    void setUserName(QString email);
    QString getUserName();

    void setPassword(QString pasword);
    QString getPassword();

signals:

public slots:
private:
    QSettings *m_settings;
};

#endif // SETTINGS_H
