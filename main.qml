import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2

Window {
    visible: true
    minimumHeight: 600
    minimumWidth: 480
    TabBar{
        id: tabs
        height:hometab.height
        TabButton{
            id: hometab;
            text: "Home"
        }
        TabButton{
            text: "Options"
        }
    }
    Toast{
        id:toast
    }

    Loader{
        id:loader
        active: true
        anchors{
            top: tabs.bottom
            bottom: parent.bottom;
            left: parent.left;
            right : parent.right
        }
        sourceComponent: tabs.currentIndex == 0 ? homeComp : settingsComp
    }

    Loader{
        id:loader2

        z: parent.z +1
        anchors{
            top: parent.top
             left: parent.left
             right: parent.right
             bottom: parent.bottom
        }

//        visible: true

    }

    Component{
        id: homeComp

        Home{

        }

    }
    Component{
        id:settingsComp
        Settings{

        }
    }
    Component{
        id: login
        EmailLogin{

        }
    }

    Connections{
        target: app
        onNeedLogin:{
            loader2.sourceComponent = login
        }
        onUimessage:{
            toast.show(messge);
        }
    }


}

