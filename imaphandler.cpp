#include "imaphandler.h"

#include "myoutputstreamadapter.h"

#include "example6_certificateVerifier.hpp"

#include "example6_tracer.hpp"
#include "example6_authenticator.hpp"
#include <QDebug>
#include <QTextCodec>
#include "Google/auth.h"



#include <functional>

#define qdebug qDebug()

using namespace std;
using namespace vmime::net::imap;

imapHandler::imapHandler(QObject *parent) : QObject(parent)
{

    qRegisterMetaType<allheaders>("allheaders");
    qRegisterMetaType<quidlist>("quidlist");
    qRegisterMetaType<messagelist>("messagelist");


    g_session = vmime::net::session::create();

}

imapHandler::~imapHandler()
{
    qdebug << "Destructor";
    try{
    st->disconnect();
    }catch (vmime::exception e){

    }
    timer->stop();
    delete timer;
}

QList<quint64> imapHandler::getUids(int start, int end)
{
    Q_UNUSED(end);

    start = maxuid;
    std::vector <vmime::shared_ptr<vmime::net::message>> allMessages;
    allMessages = f->getMessages(vmime:: net :: messageSet ::byUID(start,"*"));
    QList<quint64> list;
    f->fetchMessages(allMessages , vmime:: net :: fetchAttributes ::UID) ;
    for (unsigned int i = 0 ; i < allMessages . size () ; i++ )
    {
        vmime::shared_ptr< vmime:: net :: message> msg = allMessages[i];
        qdebug << "Uid " << stoul(msg.get()->getUID());
        list << stoul(msg.get()->getUID());
    }

    if(uidsignalEnable)
        emit uidGot(list);
    return list;


}

void imapHandler::login(QString email ,QString passw, QString server , int port)
{

    Q_UNUSED(email);
    Q_UNUSED(passw);



    QTime time;

    time.start();

    QString _url = QString("imaps://%1:%2@%3:%4").arg(email).arg(passw).arg(server).arg(port);
    qdebug << "url to connect " << _url;

    vmime::utility::url url(_url.toStdString());  
    st = g_session->getStore(url);



    //     Enable TLS support if available
    st->setProperty("connection.tls", true);

    // Set the time out handler
    //We need to get the timeoutHandler So we can cancel anything that the vmime library is doing so we can delete the thread in time.
    //this is done by using std::function and std::bind.
    // It's like a C callback but on steriods, we can also have the callback inside an object;
     std::function<void(shared_ptr <timeoutHandler>)> func;
     func = std::bind(&imapHandler::timeoutHCreated,this,std::placeholders::_1);
    thf = vmime::make_shared <timeoutHandlerFactory>(func);
    st->setTimeoutHandlerFactory(thf);

    // Set the object responsible for verifying certificates, in the
    // case a secured connection is used (TLS/SSL)
    st->setCertificateVerifier(vmime::make_shared <interactiveCertificateVerifier>());

    // Trace communication between client and server
    //    vmime::shared_ptr <std::ostringstream> traceStream = vmime::make_shared <std::ostringstream>();
    //    st->setTracerFactory(vmime::make_shared <myTracerFactory>(traceStream));

    // Connect to the mail store


    if(!logintoStore(st))
        return;

    // Display some information about the connection
    vmime::shared_ptr <vmime::net::connectionInfos> ci = st->getConnectionInfos();
    std::cout << std::endl;
    std::cout << "Connected to '" << ci->getHost() << "' (port " << ci->getPort() << ")" << std::endl;
    std::cout << "Connection is " << (st->isSecuredConnection() ? "" : "NOT ") << "secured." << std::endl;

    try{
        f= st->getDefaultFolder();

        f->open(vmime::net::folder::MODE_READ_ONLY);
    }catch(vmime::exception ex){
        qdebug <<  ex.name() << ex.what();
        return;
    }catch(exception e){
        qdebug << "could not open defult folder";
        return;
    }
    qdebug << "Login needed" << time.elapsed()/1000.0 << "secs";

    size_t max,un;
    f->status(max,un);
    totalmsgs = max;

    SearchForHeaders();
    //    return;

    if(!timer){
        timer = new QTimer(this);
    }


    connect(timer,SIGNAL(timeout()),this,SLOT(checkfornewmails()));
    timer->start(1000*60);


    //    //---------------------------------//
    //    getMails(uids);
    //    //--------------------------------//

    //    qdebug << "Get All emails elapsed " << time.elapsed()/1000.0;

    emit loginStateChanged(true);
    //    connect (this,SIGNAL(newmails(QList<quint64>)),this,SLOT(getHeaders(QList<quint64>)));
    //    getHeaders(getUids());
    return;
}

bool imapHandler::logintoStore(vmime::shared_ptr <vmime::net::store> &st)
{
    try{
        st->connect();
    }catch(vmime::exceptions::authentication_error au){
        qdebug << "Wrong Username or pass";
        emit uimessage("Wrong Username or pass");
        emit loginStateChanged(false);
        return false;
    }catch(vmime::exceptions::already_connected){
        qdebug << "Allready Connected!";
        st->disconnect();
        logintoStore(st);
        //emit loginStateChanged(true);
    }catch(vmime::exceptions::operation_timed_out){
        logintoStore(st);
    }
    catch(vmime::exception es){
        qdebug << es.what();
    }
    catch (std::exception e){
        qdebug << e.what();
        emit loginStateChanged(false);
        return false;
    }
    return true;
    emit loginStateChanged(true);
}


const QMap<quint64, QStringList> imapHandler::getHeaders(QList<quint64> uids)
{

    qdebug <<"Get Headers";
    qdebug << "uid size "<< uids.size();

    if(uids.isEmpty()){
        uids = getUids(maxuid);
    }

    //We need to reconstruct the uids, to std C++ so it can be unserstood by the library

    vector<vmime::net::message::uid> uidlist;

    foreach(quint64 uid, uids){
        qdebug << "foreach" << uid;
        uidlist.push_back(vmime::net::message::uid (uid));
    }
    //It ends here

    vmime::net::messageSet mset(vmime::net::messageSet::byUID(uidlist));

    vmime::net::fetchAttributes attribs;
    attribs.add(vmime::net::fetchAttributes::FULL_HEADER);
    //    attribs.add("X-note-share-alpha-ptx");

    QMap<quint64,QStringList> all;

    std::vector <vmime::shared_ptr <vmime::net::message> >  msgs = f->getAndFetchMessages(mset, attribs);

    //Reconstruct the vmime structure to a Qt structure and simplify things.
    for(unsigned int i=0;i<msgs.size();i++){
        qdebug << "for header";
        shared_ptr<const vmime::header> hdr =  msgs.at(i).get()->getHeader();
        const vector<shared_ptr<const vmime::headerField> > hdrlist =  hdr.get()->getFieldList();
        quint64 uid = stoul(msgs.at(i)->getUID());
        all[uid];
        for(quint32 i=0 ; i<hdrlist.size();i++){
            all[uid].append(QString::fromStdString(hdrlist.at(i)->generate())); //Iterate all over the gotten headers and put them in a list.
        }
    }
    qdebug << "emit headersGot(all)";
    emit headersGot(all);


    return all; // Return the map, with key the uid and value containing a QStringList with the headers.
}

const QMap<quint64, QStringList> imapHandler::getHeaders()
{
    uidsignalEnable = false;
    QMap<quint64, QStringList> temp = getHeaders(getUids(maxuid));
    uidsignalEnable = true;
    return temp;
}

QList<Message> imapHandler::getMails(quidlist uids)
{
    QList<Message> mails;
    for(int i=0;i<uids.size();i++){
        mails << dogetMails(uids.at(i));
    }
    emit mailsGot(mails);
    return mails;
}

void imapHandler::SearchForHeaders(QString header)
{

    qdebug << "SearchForHeaders uid" << maxuid;

    shared_ptr<vmime::net::imap::IMAPConnection> conn = f->getImapConnection();
    QString cmd;

    //    cmd = QString("A284 UID SEARCH HEADER '%1' UID %2:* \r\n").arg(header).arg(QString::number(maxuid));
    //cmd = QString("284 UID SEARCH ALL HEADER \"%1\" AND UID %2:* \r\n").arg(header).arg(maxuid);
    //qdebug << "Command to Send" << cmd;
    shared_ptr <vmime::net::socket> sock = conn->getSocket();
    vmime::byte_t m_buffer[65536];
    size_t bytesread ;
    int i=0;
    QString resp;
    quidlist uids;
    bool needsCollander = false; //Extra filtering in case get the uid list from Subject search. We may have an Impostor!
    if( resp.contains("No",Qt::CaseInsensitive) || resp.contains("Bad",Qt::CaseInsensitive) || resp.isEmpty() ){
        qdebug << "Use subject";
        cmd = QString("285 UID SEARCH ALL SUBJECT \"nsa test mail\" UID %1:* \r\n").arg(maxuid+1);
        qdebug << cmd;
        try{
            conn->sendRaw(reinterpret_cast<const vmime::byte_t*>(cmd.toLatin1().constData()),(size_t)cmd.size());
        }catch(vmime::exception e){
            qdebug << "Failed to send Search command!!";
            return;
        }
        i=0;
        while(i<3){
            try{
                sock->waitForRead();

                bytesread =  sock->receiveRaw(m_buffer, sizeof(m_buffer));
            }catch(vmime::exceptions::operation_timed_out e){
                //The server may respond slowly or be bussy with the command, try 1 more time;
                conn->sendRaw(reinterpret_cast<const vmime::byte_t*>(cmd.toLatin1().constData()),(size_t)cmd.size());
                qdebug << "Resend";
                i++;
            }
            break;
        }
        resp = QByteArray(reinterpret_cast<char*>(m_buffer),bytesread);

        qdebug << "Subject response " << resp;

//        if(resp.contains("No",Qt::CaseInsensitive) || resp.contains("Bad",Qt::CaseInsensitive) || resp.isEmpty()){
//            //Server does not support search, switching to manual search.
//            getHeaders();
//            return;
//        }
//        needsCollander = false;
    }

    if(resp.startsWith("*")){
        QStringList reslis;
        reslis = resp.split("\r\n");
        for(QString e : reslis){
            if(e.startsWith("*")){
                e = e.remove("* SEARCH").trimmed();
                QStringList u = e.split(" ");
                for(QString i : u){
                    uids << i.toULong();
                    qdebug << "uid" << i.toULong();
                }
            }
        }
    }

    qdebug << uids;
    if(uids.size()==1 && uids[0]==0){
        qdebug << "It should return";
        return;
    }



    setMaxuid(getmax(uids));
    if(needsCollander){
        qdebug << "It needs extra filtering";
        getHeaders(uids);
        return;
    }
    getMails(uids);
}

Message imapHandler::dogetMails(const quint64 uid)
{

    vmime::net::messageSet mset(vmime::net::messageSet::byUID(uid));
    vmime::net::fetchAttributes attribs;
    attribs.add(vmime::net::fetchAttributes::ENVELOPE);
    qdebug << "Fetch and get" << uid;
    std::vector<vmime::shared_ptr <vmime::net::message>> msgs;
    int i=0;
    while(i<2){
        try{
            msgs =  f->getAndFetchMessages(mset, attribs);
        }catch (vmime::exceptions::operation_timed_out){
            qdebug << "Time out, try to get msg";
            i++;
            continue;
        }catch (vmime::exception e){
            qdebug << Q_FUNC_INFO << "Exception " << e.what();
        }

        break;
    }
    if(i==2){
        exit(-1);
    }

    qdebug << "msgs size " << msgs.size();
    vmime::shared_ptr <vmime::message> parsedMsg = msgs.at(0)->getParsedMessage();
    vmime::messageParser mp(parsedMsg);

    Message _message ;
    _message.setFrom(QString::fromStdString(mp.getExpeditor().getEmail().generate()));

    vmime::addressList cclist = mp.getCopyRecipients();
    QStringList cc;
    for(size_t x=0;x<cclist.getAddressCount();x++){
        cc << QString::fromStdString(cclist.getAddressAt(x)->generate());
    }
    _message.setCc(cc);

    qdebug << "From:" << _message.getFrom();

    for(size_t z=0;z<mp.getTextPartCount();++z){
        //        qdebug << "FOR";
        vmime::shared_ptr <const vmime::textPart> tp = mp.getTextPartAt(z);
        //        qdebug << "Text parts got";
        //        qdebug << "Type " << QString::fromStdString(tp->getType().getSubType());
        string sub = tp->getType().getSubType();
        bool enterif = sub == vmime::mediaTypes::TEXT_PLAIN ? true : false;
        if(enterif){
            size_t size =0;
            //            qdebug << "Osa";
            myoutputStreamAdapter osa (size);
            //            qdebug << "Extract";
            tp->getText()->extract(osa);
            //            qdebug << "returned size " << size;
            QTextCodec * codec = QTextCodec::codecForName(tp->getCharset().generate().c_str());
            if(codec!=0){
                   _message.setText(codec->toUnicode(osa.getChars()));
            }else{
                  _message.setText(QString::fromLatin1(osa.getChars()));
            }


        }
    }
    //    qdebug << "Attachemnts";
    size_t attachcount = mp.getAttachmentCount();
    //    qdebug << "Attachment count" << attachcount;
    for(size_t z=0;z<attachcount;++z){
        vmime::shared_ptr <const vmime::attachment> at = mp.getAttachmentAt(z);
        size_t size =0;
        myoutputStreamAdapter osa (size);

        at->getData()->extract(osa);
        //        qdebug << "Attachment filename " <<  QString::fromStdString(at->getName().generate());
        //            qdebug << "Attachment" << att;
        _message.changelog << osa.getChars();

    }

    QString rec = QString::fromStdString(mp.getRecipients().getAddressAt(0)->generate());
    int tmp = rec.indexOf("<");
    rec =  rec.mid(tmp+1);
    rec.chop(1);
    _message.setRecipient( rec);
    qdebug << "Recipient " << _message.getRecipient();
    //    qdebug << "Return";
    return _message;
}

quint64 imapHandler::getmax(QList<quint64> uids)
{
    quint64 temp =0;
    for(int i=0;i<uids.size();i++){
        if(uids[i] > temp){
            temp = uids[i];
        }
    }
    qdebug << "maxuid" << temp;
    return temp;
}

quint64 imapHandler::getMaxuid() const
{
    return maxuid;
}

void imapHandler::setMaxuid(const quint64 value)
{
    qdebug << "Max Uid set!" << value;
    maxuid = value;
}

void imapHandler::cancelOpration()
{
    qdebug << Q_FUNC_INFO;
    th->cancel();
}

void imapHandler::timeoutHCreated(shared_ptr<timeoutHandler> _th)
{
    qdebug << Q_FUNC_INFO;
    th = _th;
}

void imapHandler::checkfornewmails()
{
    qdebug << "Check for new mails";
    size_t max,un;
    try{
    f->status(max,un);
    }catch(vmime::exception ){
        return;
    }

    if(max>totalmsgs){
        try{
            SearchForHeaders();

        }catch(vmime::exceptions::operation_timed_out){
            return;
        }catch(vmime::exception e){
            qdebug << "Exception " << e.what();
        }
    }
}
