#ifndef USERMODEL_H
#define USERMODEL_H

#include <QAbstractListModel>
#include <QStringList>

class userModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit userModel(QObject *parent = 0);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    void setUserlist(QStringList *value);

    Q_INVOKABLE insert();

private:
    QStringList *userlist;
};

#endif // USERMODEL_H
