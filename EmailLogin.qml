import QtQuick 2.5
import QtQuick.Controls 2.0

Rectangle {
    anchors.fill: parent
    //    color: "red"
    MouseArea{
        anchors.fill: parent
    }
    Rectangle{
        id: indicator_rec
        z:1
        visible: false
        color: Qt.rgba(0.5,0.5,0.5,0.5);
        anchors.fill: parent
        MouseArea{
            enabled: parent.visible
            anchors.fill: parent
        }
        BusyIndicator{
            id: indicator
            anchors.centerIn: parent
            height: 150
            width: 150
            running: indicator_rec.visible
            z: parent.z+1

        }
    }

    Item{
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: parent.right
        height: logo.height + email.height*2
        Component.onCompleted:  console.log("LOOGIN " + height)
        Image {
            id: logo
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            source: "qrc:/Images/Logo.png"
        }
        TextField{
            id: email
            anchors.top: logo.bottom
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 15
            anchors.rightMargin: 15
            placeholderText: "Email"
        }
        TextField{
            id: pass
            anchors.left: parent.left
            echoMode: "Password"
            anchors.right: parent.right
            anchors.leftMargin: 15
            anchors.rightMargin: 15
            anchors.top: email.bottom
            anchors.topMargin: 5
            placeholderText: "Password"
        }
        Button{
            id: button
            anchors.top: pass.bottom
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter;
            text: "Login"
            onClicked: {
                app.login(email.text , pass.text)
                indicator_rec.visible= true
            }
        }
    }
    Connections{
        target: app
        onLoginstateChanged:{
            if(state)
            {
                loader2.sourceComponent = undefined
            }else{
                indicator_rec.visible = false;
            }
        }
    }
}
