#include "myoutputstreamadapter.h"

#include <QDebug>

#define qdebug qDebug()


myoutputStreamAdapter::myoutputStreamAdapter(size_t &sizeout)
{
    size = &sizeout;
}

void myoutputStreamAdapter::flush()
{

}
void myoutputStreamAdapter::writeImpl(const vmime::byte_t * const data, const size_t count)
{
//    qdebug << Q_FUNC_INFO;
//    qdebug << "Size got="<<count;
    QByteArray arr = QByteArray::fromRawData(reinterpret_cast <const char*>(data),count); //reinterpret_cast <const char*>(data);
    *size += count;
    chars.append(arr);
}

QByteArray myoutputStreamAdapter::getChars() const
{
    return chars;
}
