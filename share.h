#ifndef SHARE_H
#define SHARE_H

#include <QString>
#include <QList>
#include <QMap>
#include <QDataStream>

class Share
{
public:
    Share();

     unsigned int version = 0;
     bool edited =false;
     int id =0;
     

     QString subject;
     QString note;
//     QMap<QString, bool> todo;
     QList<QPair<QString,bool> >todo;

     QString creator;
     QStringList users;

};
 QDataStream &operator >> (QDataStream &in , Share &sh);
 QDataStream &operator << (QDataStream &out, Share &sh);
 typedef QList<Share> sharelist;
#endif // SHARE_H
