#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "notemanager.h"

#include "share.h"
#include <QByteArray>
#include "debug.h"

#include <QDataStream>

#include "smtp/SmtpMime"

#include <applicationui.h>


//#include <QCoreApplication>


int main(int argc, char *argv[])
{


    QGuiApplication app(argc, argv);

    ApplicationUi appui;
    app.connect(&app,&QGuiApplication::aboutToQuit,&appui,&ApplicationUi::exiting);


    return app.exec();
}

