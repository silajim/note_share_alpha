#include "settings.h"
#include "debug.h"

Settings::Settings(QObject *parent) : QObject(parent)
{
    m_settings = new QSettings("Monte_Ptixiakh",QSettings::IniFormat);
    qdebug << "Settings save location" << m_settings->fileName();
}

quint64 Settings::getMaxUid()
{

    return m_settings->value("MaxUid",1).toULongLong();
}

void Settings::setMaxUid(quint64 uid)
{
    m_settings->setValue("MaxUid",uid);
}

void Settings::setUserName(QString email)
{
    m_settings->setValue("email",email);
}

QString Settings::getUserName()
{
    return m_settings->value("email","").toString();
}

void Settings::setPassword(QString pasword)
{
    qdebug << " set Password" << pasword;
    m_settings->setValue("pass", pasword);
}

QString Settings::getPassword()
{
    qdebug << "read Password" << m_settings->value("pass","").toByteArray();
    return m_settings->value("pass","").toString();
}
