#-------------------------------------------------
#
# Project created by QtCreator 2011-08-11T20:59:25
#
#-------------------------------------------------

QT       += core network

#TARGET = SMTPEmail

# Build as an application
#TEMPLATE = app

# Build as a library
#TEMPLATE = lib
DEFINES += SMTP_BUILD
#CONFIG += staticlib
#CONFIG += lib

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD



SOURCES += \
    $$PWD/emailaddress.cpp \
    $$PWD/mimeattachment.cpp \
    $$PWD/mimefile.cpp \
    $$PWD/mimehtml.cpp \
    $$PWD/mimeinlinefile.cpp \
    $$PWD/mimemessage.cpp \
    $$PWD/mimepart.cpp \
    $$PWD/mimetext.cpp \
    $$PWD/smtpclient.cpp \
    $$PWD/quotedprintable.cpp \
    $$PWD/mimemultipart.cpp \
    $$PWD/mimecontentformatter.cpp \

HEADERS  += \
    $$PWD/emailaddress.h \
    $$PWD/mimeattachment.h \
    $$PWD/mimefile.h \
    $$PWD/mimehtml.h \
    $$PWD/mimeinlinefile.h \
    $$PWD/mimemessage.h \
    $$PWD/mimepart.h \
    $$PWD/mimetext.h \
    $$PWD/smtpclient.h \
    $$PWD/SmtpMime \
    $$PWD/quotedprintable.h \
    $$PWD/mimemultipart.h \
    $$PWD/mimecontentformatter.h \
    $$PWD/smtpexports.h
