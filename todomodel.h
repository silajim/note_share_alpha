#ifndef TODOMODEL_H
#define TODOMODEL_H

#include "debug.h"
#include <QAbstractListModel>

class todoModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit todoModel(QObject *parent = 0);

    enum NoteRoles{
        todoNameRole = Qt::UserRole+1,
        todoDoneRole
    };

  QHash<int, QByteArray> roleNames() const ;

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
  Q_INVOKABLE  bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;    



    void setTodo(QList<QPair<QString, bool> > *value);

public slots:
    void addTodo();

private:

    QList<QPair<QString,bool> > *todo;

};

#endif // TODOMODEL_H
