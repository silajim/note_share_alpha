#include "readsharemodel.h"

ReadShareModel::ReadShareModel(QObject *parent) : QObject(parent)
{
//    qRegisterMetaType<QStringList*>("QStringList*");
}

Share ReadShareModel::getShare() const
{
    return sh;
}

void ReadShareModel::setShare(Share value)
{
    qdebug << "Share set!";
    sh = value;
    todomodel.setTodo(&sh.todo);
    usermodel.setUserlist(&sh.users);
    emit ListModelChanged();
}

quint32 ReadShareModel::Version()
{
    return sh.version;
}

int ReadShareModel::Id()
{
    return sh.id;
}

QString ReadShareModel::Subject()
{

    return sh.subject;
}

QString ReadShareModel::Note()
{
    return sh.note;
}

QString ReadShareModel::Creator()
{
    return sh.creator;
}

QStringList ReadShareModel::Users()
{
    return sh.users;
}

todoModel *ReadShareModel::listModel()
{
    return &todomodel;
}

void ReadShareModel::setSubject(QString &value)
{
    qdebug << "subject changed " << value;
    sh.subject=value;
    emit SubjectChanged();
}

void ReadShareModel::setNote(QString &value)
{
    sh.note = value;
    emit NoteChanged();
}

void ReadShareModel::setCreator(QString &value)
{
    sh.creator = value;
    emit CreatorChanged();
}

void ReadShareModel::setUsers(QStringList value)
{
    sh.users = value;
    emit UsersChanged();
}

void ReadShareModel::save()
{
   qdebug << "Users " << sh.users;
    sh.version++;
    emit Save(sh);
}

userModel* ReadShareModel::getUsermodel()
{
    return &usermodel;
}
