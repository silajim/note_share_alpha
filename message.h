#ifndef MESSAGE_H
#define MESSAGE_H

#include <QString>
#include <QList>
#include <QByteArray>

class Message
{
public:
    Message();
    QList<QByteArray> getChangelog();
    void setChangelog(const QList<QByteArray> value);

    QString getFrom() const;
    void setFrom(const QString value);

    QString getText() const;
    void setText(const QString value);

     QList<QByteArray> changelog;

     QStringList getCc() const;
     void setCc(const QStringList value);

     QString getRecipient() const;
     void setRecipient(const QString &value);

private:
     QString from;
     QStringList cc;
     QString text;
     QString recipient;

};

#endif // MESSAGE_H
