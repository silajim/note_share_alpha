#ifndef NOTEMODEL_H
#define NOTEMODEL_H

#include <QString>
#include <QList>
#include <QAbstractListModel>
#include <QVariant>
#include "share.h"

class NotemodelData: public QObject{

    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString creator READ creator WRITE setCreator NOTIFY creatorChanged)

public:
    QString name;
    QString creator;
    int version;
    int id;
    QString getName() const;
    void setName(const QString &value);
    QString getCreator() const;
    void setCreator(const QString &value);

signals:
    void nameChanged();
    void creatorChanged();

};


class NoteModel: public QAbstractListModel
{
public:

    enum NoteRoles{
        creatorRole = Qt::UserRole+1,
        nameRole,
        versionRole,
        idRole
    };



    NoteModel();

    QHash<int, QByteArray> roleNames() const ;

    QVariant data(const QModelIndex &index, int role) const;

    int rowCount(const QModelIndex &parent) const;

    void insertShare(Share &sh);

    void updateShare(Share &sh);

private:
    QList<NotemodelData*> modeldata;

};


#endif // NOTEMODEL_H
