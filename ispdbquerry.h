#ifndef ISPDBQUERRY_H
#define ISPDBQUERRY_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QList>
#include <server.h>
#include <QTcpSocket>


class ispdbQuerry : public QObject
{
    Q_OBJECT
public:
    explicit ispdbQuerry(QObject *parent = 0);
    ~ispdbQuerry();
    void querry(QString email);

signals:
    void information(QList<server*>);
public slots:
    void result(QNetworkReply *reply);
     void imapCheck();
     void imapCheckFailed(QAbstractSocket::SocketError error);
     void smtpOk();
     void smtpFailed(QAbstractSocket::SocketError error);
    private:
    QString prov;
    QNetworkAccessManager *manager;

     QNetworkReply *reply;

};

#endif // ISPDBQUERRY_H
