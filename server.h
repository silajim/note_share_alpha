#ifndef SERVER_H
#define SERVER_H

#include <QString>

class server
{


public:
    server();
    ~server();

    enum class Auth: int{
        OAuth2 = 0,
        Standard
    };

    QString getEmail_provider() const;
    void setEmail_provider(const QString &value);

    QString getAddress() const;
    void setAddress(const QString &value);

    int getPort() const;
    void setPort(const int &value);

    Auth getAuthentication() const;
    void setAuthentication(const Auth &value);

    QString getSocketType() const;
    void setSocketType(const QString &value);

private:

    QString email_provider;
    QString address;
    QString socketType;
    int port;
    Auth authentication = Auth::Standard;
};

#endif // SERVER_H
